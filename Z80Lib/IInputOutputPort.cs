﻿namespace Z80Lib {
    public interface IInputOutputPort {
        byte Read(Z80 cpu, ushort addr);
        void Write(Z80 cpu, ushort addr, byte value);
    }
}
