﻿#define LITTLE_ENDIAN

using System.Runtime.InteropServices;

namespace Z80Lib {
#if (LITTLE_ENDIAN)
    [StructLayout(LayoutKind.Explicit)]
    public struct RegPair {
        [FieldOffset(0)]
        public ushort w;

        [FieldOffset(0)]
        public byte l;
        [FieldOffset(1)]
        public byte h;

        #region shortcuts for more readable field access
        [FieldOffset(0)]
        public byte f;
        [FieldOffset(1)]
        public byte a;

        [FieldOffset(0)]
        public byte c;
        [FieldOffset(1)]
        public byte b;

        [FieldOffset(0)]
        public byte e;
        [FieldOffset(1)]
        public byte d;
        #endregion

        #region operators
        public static bool operator ==(RegPair a, RegPair b) {
            return a.w == b.w;
        }

        public override bool Equals(object o) {
            try {
                return this == (RegPair)o;
            } catch {
                return false;
            }
        }

        public override int GetHashCode() {
            return w;
        }

        public static bool operator !=(RegPair a, RegPair b) {
            return a.w != b.w;
        }
        #endregion

        public override string ToString() {
            return string.Format("{0:X4}", w);
        }
    }
#endif
}