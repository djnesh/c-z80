﻿namespace Z80Lib {
    public interface IIRQRead {
        byte Read(Z80 cpu);
    }
}
