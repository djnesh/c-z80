﻿using System;

namespace Z80Lib {
    public sealed partial class Z80 {
        public event TStateEvent TState;
        public event RetiEvent Reti;

        // opcode delegate and table size
        delegate void OperationDelegate();

        Context cpu;

        public Z80(Context context) {
            cpu = context;
            init_opcodes();
        }

        /**
         * Create (internal) context from components
         */
        public Z80(IMemory memory, IInputOutputPort inputOutputPortPorts, IIRQRead irqRead, bool reset = true) {
            cpu = new Context {
                mem = memory,
                io = inputOutputPortPorts,
                intread = irqRead
            };

            init_opcodes();
            if(reset) Reset();
        }

        void init_opcodes() {
            init_opcodes_base();
            init_opcodes_cb();
            init_opcodes_dd();
            init_opcodes_ddcb();
            init_opcodes_ed();
            init_opcodes_fd();
            init_opcodes_fdcb();
        }

        public bool InstructionDone() {
            return cpu.prefix == 0;
        }

        public ulong Step() {
            cpu.doing_opcode = true;
            cpu.noint_once = false;
            cpu.reset_PV_on_int = false;
            cpu.tstate = 0;
            cpu.op_tstate = 0;

            byte opcode = READ_OP_M1();
            if(cpu.int_vector_req != 0) TSTATES(2); // interrupt eats two extra wait-states
            cpu.r++;

            T_WAIT_UNTIL(4); // M1 cycle eats min 4 t-states

            if(cpu.prefix == 0) {
                opcodes_base[opcode]();
            } else {
                if((cpu.prefix | 0x20) == 0xFD && ((opcode | 0x20) == 0xFD || opcode == 0xED)) {
                    cpu.prefix = opcode;
                    cpu.noint_once = true; // interrupts are not accepted immediately after prefix
                } else {
                    OperationDelegate ofn;

                    switch(cpu.prefix) {
                        case 0xDD:
                        case 0xFD:
                            if(opcode == 0xCB) {
                                byte d = READ_OP();
                                cpu.tmpbyte_s = (sbyte)((d & 0x80) != 0 ? -(((~d) & 0x7f) + 1) : d);
                                opcode = READ_OP();
                                ofn = cpu.prefix == 0xDD ? opcodes_ddcb[opcode] : opcodes_fdcb[opcode];
                            } else {
                                ofn = (cpu.prefix == 0xDD ? opcodes_dd[opcode] : opcodes_fd[opcode]) ??
                                      opcodes_base[opcode];
                            }
                            break;

                        case 0xED:
                            ofn = opcodes_ed[opcode] ?? opcodes_base[0x00];
                            break;

                        case 0xCB:
                            ofn = opcodes_cb[opcode];
                            break;

                        default:
                            // this mustn't happen!
                            throw new SystemException(string.Format("Invalid prefix {0:X2}", cpu.prefix));
                    }
                    ofn();
                    cpu.prefix = 0;
                }
            }

            cpu.doing_opcode = false;
            return cpu.tstate;
        }

        public Context getContext() {
            return cpu;
        }

        public void Reset() {
            cpu.Reset();
        }

        #region IRQ

        public ulong Nmi() {
            if(cpu.doing_opcode || cpu.noint_once || (cpu.prefix != 0)) return 0;

            if(cpu.halted) { cpu.pc.w++; cpu.halted = false; } // so we met an interrupt... stop waiting

            cpu.doing_opcode = true;

            cpu.r++; // accepting interrupt increases R by one

            // IFF2=IFF1
            // contrary to zilog z80 docs, IFF2 is not modified on NMI. proved by Slava Tretiak aka restorer
            cpu.iff1 = 0;

            TSTATES(5);

            cpu.mem.Write(this, --cpu.sp.w, cpu.pc.h); // PUSH PC -- high byte
            TSTATES(3);

            cpu.mem.Write(this, --cpu.sp.w, cpu.pc.l); // PUSH PC -- low byte
            TSTATES(3);

            cpu.pc.w = 0x0066;
            cpu.memptr.w = cpu.pc.w; // FIXME: is that really so?

            cpu.doing_opcode = false;

            return 11; // NMI always takes 11 t-states
        }

        public ulong Int() {
            // If the INT line is low and cpu.iff1 is set, and there's no opcode executing just now,
            // a maskable interrupt is accepted, whether or not the
            // last INT routine has finished
            if((cpu.iff1 == 0) || cpu.noint_once || cpu.doing_opcode || (cpu.prefix != 0)) return 0;

            cpu.tstate = 0;
            cpu.op_tstate = 0;

            if(cpu.halted) { cpu.pc.w++; cpu.halted = false; } // so we met an interrupt... stop waiting

            // When an INT is accepted, both cpu.iff1 and IFF2 are cleared, preventing another interrupt from
            // occurring which would end up as an infinite loop
            cpu.iff1 = cpu.iff2 = 0;

            // original (NMOS) zilog z80 bug:
            // If a LD A,I or LD A,R (which copy IFF2 to the P/V flag) is interrupted, then the P/V flag is reset, even if interrupts were enabled beforehand.
            // (this bug was fixed in CMOS version of z80)
            if(cpu.reset_PV_on_int) { cpu.af.f = (byte)(cpu.af.f & ~Tables.FLAG_P); }
            cpu.reset_PV_on_int = false;

            cpu.int_vector_req = 1;
            cpu.doing_opcode = true;

            switch(cpu.im) {
                case IM_MODE.IM0:
                    // note: there's no need to do R++ and WAITs here, it'll be handled by z80ex_step
                    ulong tt = Step();

                    while(cpu.prefix != 0) { // this is not the end?
                        tt += Step();
                    }

                    cpu.tstate = tt;
                    break;

                case IM_MODE.IM1:
                    cpu.r++;
                    TSTATES(2); // two extra wait-states
                    // An RST 38h is executed, no matter what value is put on the bus or what
                    // value the I register has. 13 t-states (2 extra + 11 for RST).
                    opcodes_base[0xFF](); // RST38
                    break;

                case IM_MODE.IM2:
                    cpu.r++;
                    // takes 19 clock periods to complete (seven to fetch the
                    // lower eight bits from the interrupting device, six to save the program
                    // counter, and six to obtain the jump address)
                    byte iv = READ_OP();
                    T_WAIT_UNTIL(7);
                    ushort inttemp = (ushort)((0x100 * cpu.i) + iv);

                    PUSH(cpu.pc, 7, 10);

                    cpu.pc.l = READ_MEM(inttemp++, 13); cpu.pc.h = READ_MEM(inttemp, 16);
                    cpu.memptr.w = cpu.pc.w;
                    T_WAIT_UNTIL(19);

                    break;
            }

            cpu.doing_opcode = false;
            cpu.int_vector_req = 0;

            return cpu.tstate;
        }

        #endregion

        #region memory and port read / write
        /**
         * read opcode
         */
        byte READ_OP_M1() {
            return cpu.int_vector_req != 0 ? cpu.intread.Read(this) : cpu.mem.Read(this, cpu.pc.w++, true);
        }

        /**
         * read opcode argument
         */
        byte READ_OP() {
            return cpu.int_vector_req != 0 ? cpu.intread.Read(this) : cpu.mem.Read(this, cpu.pc.w++);
        }

        /**
         * wait until end of opcode-tstate given (to be used on opcode execution).
         */
        public void T_WAIT_UNTIL(int t_state) {
            TStateEvent handler = TState;

            if(handler == null) {
                if(t_state <= cpu.op_tstate) return;
                cpu.tstate += (ulong)(t_state - cpu.op_tstate);
                cpu.op_tstate = (byte)t_state;
            } else {
                for(int i = cpu.op_tstate; i < t_state; i++) {
                    cpu.op_tstate++;
                    cpu.tstate++;
                    handler(this, null);
                }
            }
        }

        /**
         * spend <amount> t-states (not affecting opcode-tstate counter,
         * for using outside of certain opcode execution)
         */
        public void TSTATES(int amount) {
            TStateEvent handler = TState;

            if(handler == null) {
                cpu.tstate += (ulong)amount;
            } else {
                for(int i = cpu.op_tstate; i < amount; i++) {
                    cpu.tstate++;
                    handler(this, null);
                }
            }
        }

        /**
         * read byte from memory
         */
        byte READ_MEM(ushort addr, int t_state) {
            T_WAIT_UNTIL(t_state);
            return cpu.mem.Read(this, addr);
        }

        /**
         * read byte from port
         */
        byte READ_PORT(ushort port, int t_state) {
            T_WAIT_UNTIL(t_state);
            return cpu.io.Read(this, port);
        }

        /**
         * write byte to memory
         */
        void WRITE_MEM(ushort addr, byte val, int t_state) {
            T_WAIT_UNTIL(t_state);
            cpu.mem.Write(this, addr, val);
        }

        /**
         * write byte to port
         */

        void WRITE_PORT(ushort port, byte val, int t_state) {
            T_WAIT_UNTIL(t_state);
            cpu.io.Write(this, port, val);
        }
        #endregion
    }
}
