﻿namespace Z80Lib {
    partial class Z80 {
        OperationDelegate[] opcodes_fd;

        void init_opcodes_fd() {
            opcodes_fd = new OperationDelegate[] {
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_FD_0x09, null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_FD_0x19, null      , null      ,
                null      , null      , null      , null      ,
                null      , op_FD_0x21, op_FD_0x22, op_FD_0x23,
                op_FD_0x24, op_FD_0x25, op_FD_0x26, null      ,
                null      , op_FD_0x29, op_FD_0x2a, op_FD_0x2b,
                op_FD_0x2c, op_FD_0x2d, op_FD_0x2e, null      ,
                null      , null      , null      , null      ,
                op_FD_0x34, op_FD_0x35, op_FD_0x36, null      ,
                null      , op_FD_0x39, null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                op_FD_0x44, op_FD_0x45, op_FD_0x46, null      ,
                null      , null      , null      , null      ,
                op_FD_0x4c, op_FD_0x4d, op_FD_0x4e, null      ,
                null      , null      , null      , null      ,
                op_FD_0x54, op_FD_0x55, op_FD_0x56, null      ,
                null      , null      , null      , null      ,
                op_FD_0x5c, op_FD_0x5d, op_FD_0x5e, null      ,
                op_FD_0x60, op_FD_0x61, op_FD_0x62, op_FD_0x63,
                op_FD_0x64, op_FD_0x65, op_FD_0x66, op_FD_0x67,
                op_FD_0x68, op_FD_0x69, op_FD_0x6a, op_FD_0x6b,
                op_FD_0x6c, op_FD_0x6d, op_FD_0x6e, op_FD_0x6f,
                op_FD_0x70, op_FD_0x71, op_FD_0x72, op_FD_0x73,
                op_FD_0x74, op_FD_0x75, null      , op_FD_0x77,
                null      , null      , null      , null      ,
                op_FD_0x7c, op_FD_0x7d, op_FD_0x7e, null      ,
                null      , null      , null      , null      ,
                op_FD_0x84, op_FD_0x85, op_FD_0x86, null      ,
                null      , null      , null      , null      ,
                op_FD_0x8c, op_FD_0x8d, op_FD_0x8e, null      ,
                null      , null      , null      , null      ,
                op_FD_0x94, op_FD_0x95, op_FD_0x96, null      ,
                null      , null      , null      , null      ,
                op_FD_0x9c, op_FD_0x9d, op_FD_0x9e, null      ,
                null      , null      , null      , null      ,
                op_FD_0xa4, op_FD_0xa5, op_FD_0xa6, null      ,
                null      , null      , null      , null      ,
                op_FD_0xac, op_FD_0xad, op_FD_0xae, null      ,
                null      , null      , null      , null      ,
                op_FD_0xb4, op_FD_0xb5, op_FD_0xb6, null      ,
                null      , null      , null      , null      ,
                op_FD_0xbc, op_FD_0xbd, op_FD_0xbe, null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_FD_0xe1, null      , op_FD_0xe3,
                null      , op_FD_0xe5, null      , null      ,
                null      , op_FD_0xe9, null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_FD_0xf9, null      , null      ,
                null      , null      , null      , null
            };
        }

        /*ADD cpu.iy.w,cpu.bc.w*/
        void op_FD_0x09() {
            ADD16(ref cpu.iy, cpu.bc);
            T_WAIT_UNTIL(11);
        }

        /*ADD cpu.iy.w,cpu.de.w*/
        void op_FD_0x19() {
            ADD16(ref cpu.iy, cpu.de);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.iy.w,@*/
        void op_FD_0x21() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            LD16(ref cpu.iy, cpu.tmpword);
            T_WAIT_UNTIL(10);
        }

        /*LD (@),cpu.iy.w*/
        void op_FD_0x22() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.iy, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }

        /*INC cpu.iy.w*/
        void op_FD_0x23() {
            INC16(ref cpu.iy);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.iy.h*/
        void op_FD_0x24() {
            INC(ref cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.iy.h*/
        void op_FD_0x25() {
            DEC(ref cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.h,#*/
        void op_FD_0x26() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.iy.h, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*ADD cpu.iy.w,cpu.iy.w*/
        void op_FD_0x29() {
            ADD16(ref cpu.iy, cpu.iy);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.iy.w,(@)*/
        void op_FD_0x2a() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.iy, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }

        /*DEC cpu.iy.w*/
        void op_FD_0x2b() {
            DEC16(ref cpu.iy);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.iy.l*/
        void op_FD_0x2c() {
            INC(ref cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.iy.l*/
        void op_FD_0x2d() {
            DEC(ref cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,#*/
        void op_FD_0x2e() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.iy.l, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*INC (cpu.iy.w+$)*/
        void op_FD_0x34() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            INC(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*DEC (cpu.iy.w+$)*/
        void op_FD_0x35() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            DEC(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD (cpu.iy.w+$),#*/
        void op_FD_0x36() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.tmpbyte, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*ADD cpu.iy.w,cpu.sp.w*/
        void op_FD_0x39() {
            ADD16(ref cpu.iy, cpu.sp);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.bc.b,cpu.iy.h*/
        void op_FD_0x44() {
            LD(ref cpu.bc.b, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b,cpu.iy.l*/
        void op_FD_0x45() {
            LD(ref cpu.bc.b, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b,(cpu.iy.w+$)*/
        void op_FD_0x46() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.bc.c,cpu.iy.h*/
        void op_FD_0x4c() {
            LD(ref cpu.bc.c, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c,cpu.iy.l*/
        void op_FD_0x4d() {
            LD(ref cpu.bc.c, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c,(cpu.iy.w+$)*/
        void op_FD_0x4e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.de.d,cpu.iy.h*/
        void op_FD_0x54() {
            LD(ref cpu.de.d, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d,cpu.iy.l*/
        void op_FD_0x55() {
            LD(ref cpu.de.d, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d,(cpu.iy.w+$)*/
        void op_FD_0x56() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.de.d, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.de.e,cpu.iy.h*/
        void op_FD_0x5c() {
            LD(ref cpu.de.e, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e,cpu.iy.l*/
        void op_FD_0x5d() {
            LD(ref cpu.de.e, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e,(cpu.iy.w+$)*/
        void op_FD_0x5e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.de.e, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.iy.h,cpu.bc.b*/
        void op_FD_0x60() {
            LD(ref cpu.iy.h, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.h,cpu.bc.c*/
        void op_FD_0x61() {
            LD(ref cpu.iy.h, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.h,cpu.de.d*/
        void op_FD_0x62() {
            LD(ref cpu.iy.h, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.h,cpu.de.e*/
        void op_FD_0x63() {
            LD(ref cpu.iy.h, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.h,cpu.iy.h*/
        void op_FD_0x64() {
            LD(ref cpu.iy.h, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.h,cpu.iy.l*/
        void op_FD_0x65() {
            LD(ref cpu.iy.h, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD H,(cpu.iy.w+$)*/
        void op_FD_0x66() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.iy.h,cpu.af.a*/
        void op_FD_0x67() {
            LD(ref cpu.iy.h, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,cpu.bc.b*/
        void op_FD_0x68() {
            LD(ref cpu.iy.l, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,cpu.bc.c*/
        void op_FD_0x69() {
            LD(ref cpu.iy.l, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,cpu.de.d*/
        void op_FD_0x6a() {
            LD(ref cpu.iy.l, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,cpu.de.e*/
        void op_FD_0x6b() {
            LD(ref cpu.iy.l, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,cpu.iy.h*/
        void op_FD_0x6c() {
            LD(ref cpu.iy.l, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.iy.l,cpu.iy.l*/
        void op_FD_0x6d() {
            LD(ref cpu.iy.l, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD L,(cpu.iy.w+$)*/
        void op_FD_0x6e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.iy.l,cpu.af.a*/
        void op_FD_0x6f() {
            LD(ref cpu.iy.l, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD (cpu.iy.w+$),cpu.bc.b*/
        void op_FD_0x70() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.bc.b);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD (cpu.iy.w+$),cpu.bc.c*/
        void op_FD_0x71() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.bc.c);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD (cpu.iy.w+$),cpu.de.d*/
        void op_FD_0x72() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.de.d);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD (cpu.iy.w+$),cpu.de.e*/
        void op_FD_0x73() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.de.e);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD (cpu.iy.w+$),H*/
        void op_FD_0x74() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.hl.h);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD (cpu.iy.w+$),L*/
        void op_FD_0x75() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.hl.l);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD (cpu.iy.w+$),cpu.af.a*/
        void op_FD_0x77() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.af.a);
            WRITE_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }

        /*LD cpu.af.a,cpu.iy.h*/
        void op_FD_0x7c() {
            LD(ref cpu.af.a, cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a,cpu.iy.l*/
        void op_FD_0x7d() {
            LD(ref cpu.af.a, cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a,(cpu.iy.w+$)*/
        void op_FD_0x7e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.af.a, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*ADD cpu.af.a,cpu.iy.h*/
        void op_FD_0x84() {
            ADD(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a,cpu.iy.l*/
        void op_FD_0x85() {
            ADD(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a,(cpu.iy.w+$)*/
        void op_FD_0x86() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            ADD(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*ADC cpu.af.a,cpu.iy.h*/
        void op_FD_0x8c() {
            ADC(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a,cpu.iy.l*/
        void op_FD_0x8d() {
            ADC(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a,(cpu.iy.w+$)*/
        void op_FD_0x8e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            ADC(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*SUB cpu.iy.h*/
        void op_FD_0x94() {
            SUB(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.iy.l*/
        void op_FD_0x95() {
            SUB(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*SUB (cpu.iy.w+$)*/
        void op_FD_0x96() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            SUB(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*SBC cpu.af.a,cpu.iy.h*/
        void op_FD_0x9c() {
            SBC(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a,cpu.iy.l*/
        void op_FD_0x9d() {
            SBC(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a,(cpu.iy.w+$)*/
        void op_FD_0x9e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            SBC(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*AND cpu.iy.h*/
        void op_FD_0xa4() {
            AND(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.iy.l*/
        void op_FD_0xa5() {
            AND(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*AND (cpu.iy.w+$)*/
        void op_FD_0xa6() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            AND(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*XOR cpu.iy.h*/
        void op_FD_0xac() {
            XOR(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.iy.l*/
        void op_FD_0xad() {
            XOR(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*XOR (cpu.iy.w+$)*/
        void op_FD_0xae() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            XOR(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*OR cpu.iy.h*/
        void op_FD_0xb4() {
            OR(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.iy.l*/
        void op_FD_0xb5() {
            OR(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*OR (cpu.iy.w+$)*/
        void op_FD_0xb6() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            OR(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*CP cpu.iy.h*/
        void op_FD_0xbc() {
            CP(cpu.iy.h);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.iy.l*/
        void op_FD_0xbd() {
            CP(cpu.iy.l);
            T_WAIT_UNTIL(4);
        }

        /*CP (cpu.iy.w+$)*/
        void op_FD_0xbe() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.iy.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.iy.w + cpu.tmpbyte_s), 12);
            CP(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }


        /*POP cpu.iy.w*/
        void op_FD_0xe1() {
            POP(ref cpu.iy, /*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*EX (cpu.sp.w),cpu.iy.w*/
        void op_FD_0xe3() {
            cpu.tmpword.l = READ_MEM((cpu.sp.w), 4);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.sp.w + 1), 7);
            EX_MPTR(ref cpu.tmpword, ref cpu.iy);
            WRITE_MEM((cpu.sp.w), cpu.tmpword.l, 11);
            WRITE_MEM((ushort)(cpu.sp.w + 1), cpu.tmpword.h, 14);
            T_WAIT_UNTIL(19);
        }

        /*PUSH cpu.iy.w*/
        void op_FD_0xe5() {
            PUSH(cpu.iy, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*JP cpu.iy.w*/
        void op_FD_0xe9() {
            JP_NO_MPTR(cpu.iy.w);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.sp.w,cpu.iy.w*/
        void op_FD_0xf9() {
            LD16(ref cpu.sp, cpu.iy);
            T_WAIT_UNTIL(6);
        }


    }
}
