﻿namespace Z80Lib {
    partial class Z80 {
        OperationDelegate[] opcodes_ddcb;

        void init_opcodes_ddcb() {
            opcodes_ddcb = new OperationDelegate[] {
                op_DDCB_0x00, op_DDCB_0x01, op_DDCB_0x02, op_DDCB_0x03,
                op_DDCB_0x04, op_DDCB_0x05, op_DDCB_0x06, op_DDCB_0x07,
                op_DDCB_0x08, op_DDCB_0x09, op_DDCB_0x0a, op_DDCB_0x0b,
                op_DDCB_0x0c, op_DDCB_0x0d, op_DDCB_0x0e, op_DDCB_0x0f,
                op_DDCB_0x10, op_DDCB_0x11, op_DDCB_0x12, op_DDCB_0x13,
                op_DDCB_0x14, op_DDCB_0x15, op_DDCB_0x16, op_DDCB_0x17,
                op_DDCB_0x18, op_DDCB_0x19, op_DDCB_0x1a, op_DDCB_0x1b,
                op_DDCB_0x1c, op_DDCB_0x1d, op_DDCB_0x1e, op_DDCB_0x1f,
                op_DDCB_0x20, op_DDCB_0x21, op_DDCB_0x22, op_DDCB_0x23,
                op_DDCB_0x24, op_DDCB_0x25, op_DDCB_0x26, op_DDCB_0x27,
                op_DDCB_0x28, op_DDCB_0x29, op_DDCB_0x2a, op_DDCB_0x2b,
                op_DDCB_0x2c, op_DDCB_0x2d, op_DDCB_0x2e, op_DDCB_0x2f,
                op_DDCB_0x30, op_DDCB_0x31, op_DDCB_0x32, op_DDCB_0x33,
                op_DDCB_0x34, op_DDCB_0x35, op_DDCB_0x36, op_DDCB_0x37,
                op_DDCB_0x38, op_DDCB_0x39, op_DDCB_0x3a, op_DDCB_0x3b,
                op_DDCB_0x3c, op_DDCB_0x3d, op_DDCB_0x3e, op_DDCB_0x3f,
                op_DDCB_0x47, op_DDCB_0x47, op_DDCB_0x47, op_DDCB_0x47,
                op_DDCB_0x47, op_DDCB_0x47, op_DDCB_0x47, op_DDCB_0x47,
                op_DDCB_0x4f, op_DDCB_0x4f, op_DDCB_0x4f, op_DDCB_0x4f,
                op_DDCB_0x4f, op_DDCB_0x4f, op_DDCB_0x4f, op_DDCB_0x4f,
                op_DDCB_0x57, op_DDCB_0x57, op_DDCB_0x57, op_DDCB_0x57,
                op_DDCB_0x57, op_DDCB_0x57, op_DDCB_0x57, op_DDCB_0x57,
                op_DDCB_0x5f, op_DDCB_0x5f, op_DDCB_0x5f, op_DDCB_0x5f,
                op_DDCB_0x5f, op_DDCB_0x5f, op_DDCB_0x5f, op_DDCB_0x5f,
                op_DDCB_0x67, op_DDCB_0x67, op_DDCB_0x67, op_DDCB_0x67,
                op_DDCB_0x67, op_DDCB_0x67, op_DDCB_0x67, op_DDCB_0x67,
                op_DDCB_0x6f, op_DDCB_0x6f, op_DDCB_0x6f, op_DDCB_0x6f,
                op_DDCB_0x6f, op_DDCB_0x6f, op_DDCB_0x6f, op_DDCB_0x6f,
                op_DDCB_0x77, op_DDCB_0x77, op_DDCB_0x77, op_DDCB_0x77,
                op_DDCB_0x77, op_DDCB_0x77, op_DDCB_0x77, op_DDCB_0x77,
                op_DDCB_0x7f, op_DDCB_0x7f, op_DDCB_0x7f, op_DDCB_0x7f,
                op_DDCB_0x7f, op_DDCB_0x7f, op_DDCB_0x7f, op_DDCB_0x7f,
                op_DDCB_0x80, op_DDCB_0x81, op_DDCB_0x82, op_DDCB_0x83,
                op_DDCB_0x84, op_DDCB_0x85, op_DDCB_0x86, op_DDCB_0x87,
                op_DDCB_0x88, op_DDCB_0x89, op_DDCB_0x8a, op_DDCB_0x8b,
                op_DDCB_0x8c, op_DDCB_0x8d, op_DDCB_0x8e, op_DDCB_0x8f,
                op_DDCB_0x90, op_DDCB_0x91, op_DDCB_0x92, op_DDCB_0x93,
                op_DDCB_0x94, op_DDCB_0x95, op_DDCB_0x96, op_DDCB_0x97,
                op_DDCB_0x98, op_DDCB_0x99, op_DDCB_0x9a, op_DDCB_0x9b,
                op_DDCB_0x9c, op_DDCB_0x9d, op_DDCB_0x9e, op_DDCB_0x9f,
                op_DDCB_0xa0, op_DDCB_0xa1, op_DDCB_0xa2, op_DDCB_0xa3,
                op_DDCB_0xa4, op_DDCB_0xa5, op_DDCB_0xa6, op_DDCB_0xa7,
                op_DDCB_0xa8, op_DDCB_0xa9, op_DDCB_0xaa, op_DDCB_0xab,
                op_DDCB_0xac, op_DDCB_0xad, op_DDCB_0xae, op_DDCB_0xaf,
                op_DDCB_0xb0, op_DDCB_0xb1, op_DDCB_0xb2, op_DDCB_0xb3,
                op_DDCB_0xb4, op_DDCB_0xb5, op_DDCB_0xb6, op_DDCB_0xb7,
                op_DDCB_0xb8, op_DDCB_0xb9, op_DDCB_0xba, op_DDCB_0xbb,
                op_DDCB_0xbc, op_DDCB_0xbd, op_DDCB_0xbe, op_DDCB_0xbf,
                op_DDCB_0xc0, op_DDCB_0xc1, op_DDCB_0xc2, op_DDCB_0xc3,
                op_DDCB_0xc4, op_DDCB_0xc5, op_DDCB_0xc6, op_DDCB_0xc7,
                op_DDCB_0xc8, op_DDCB_0xc9, op_DDCB_0xca, op_DDCB_0xcb,
                op_DDCB_0xcc, op_DDCB_0xcd, op_DDCB_0xce, op_DDCB_0xcf,
                op_DDCB_0xd0, op_DDCB_0xd1, op_DDCB_0xd2, op_DDCB_0xd3,
                op_DDCB_0xd4, op_DDCB_0xd5, op_DDCB_0xd6, op_DDCB_0xd7,
                op_DDCB_0xd8, op_DDCB_0xd9, op_DDCB_0xda, op_DDCB_0xdb,
                op_DDCB_0xdc, op_DDCB_0xdd, op_DDCB_0xde, op_DDCB_0xdf,
                op_DDCB_0xe0, op_DDCB_0xe1, op_DDCB_0xe2, op_DDCB_0xe3,
                op_DDCB_0xe4, op_DDCB_0xe5, op_DDCB_0xe6, op_DDCB_0xe7,
                op_DDCB_0xe8, op_DDCB_0xe9, op_DDCB_0xea, op_DDCB_0xeb,
                op_DDCB_0xec, op_DDCB_0xed, op_DDCB_0xee, op_DDCB_0xef,
                op_DDCB_0xf0, op_DDCB_0xf1, op_DDCB_0xf2, op_DDCB_0xf3,
                op_DDCB_0xf4, op_DDCB_0xf5, op_DDCB_0xf6, op_DDCB_0xf7,
                op_DDCB_0xf8, op_DDCB_0xf9, op_DDCB_0xfa, op_DDCB_0xfb,
                op_DDCB_0xfc, op_DDCB_0xfd, op_DDCB_0xfe, op_DDCB_0xff
            };
        }

        /*LD cpu.bc.b,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x00() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x01() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x02() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x03() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x04() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x05() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RLC (cpu.ix.w+$)*/
        void op_DDCB_0x06() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RLC (cpu.ix.w+$)*/
        void op_DDCB_0x07() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RLC(ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x08() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x09() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x0a() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x0b() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x0c() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x0d() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RRC (cpu.ix.w+$)*/
        void op_DDCB_0x0e() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RRC (cpu.ix.w+$)*/
        void op_DDCB_0x0f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RRC(ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RL (cpu.ix.w+$)*/
        void op_DDCB_0x10() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RL (cpu.ix.w+$)*/
        void op_DDCB_0x11() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RL (cpu.ix.w+$)*/
        void op_DDCB_0x12() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RL (cpu.ix.w+$)*/
        void op_DDCB_0x13() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RL (cpu.ix.w+$)*/
        void op_DDCB_0x14() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RL (cpu.ix.w+$)*/
        void op_DDCB_0x15() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RL (cpu.ix.w+$)*/
        void op_DDCB_0x16() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RL (cpu.ix.w+$)*/
        void op_DDCB_0x17() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RL(ref cpu.tmpbyte);
            LD16(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RR (cpu.ix.w+$)*/
        void op_DDCB_0x18() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RR (cpu.ix.w+$)*/
        void op_DDCB_0x19() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RR (cpu.ix.w+$)*/
        void op_DDCB_0x1a() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RR (cpu.ix.w+$)*/
        void op_DDCB_0x1b() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RR (cpu.ix.w+$)*/
        void op_DDCB_0x1c() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RR (cpu.ix.w+$)*/
        void op_DDCB_0x1d() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RR (cpu.ix.w+$)*/
        void op_DDCB_0x1e() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RR (cpu.ix.w+$)*/
        void op_DDCB_0x1f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RR(ref cpu.tmpbyte);
            LD16(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x20() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x21() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x22() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x23() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x24() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x25() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SLA (cpu.ix.w+$)*/
        void op_DDCB_0x26() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SLA (cpu.ix.w+$)*/
        void op_DDCB_0x27() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLA(ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x28() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x29() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x2a() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x2b() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x2c() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x2d() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SRA (cpu.ix.w+$)*/
        void op_DDCB_0x2e() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SRA (cpu.ix.w+$)*/
        void op_DDCB_0x2f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRA(ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x30() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x31() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x32() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x33() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x34() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x35() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SLL (cpu.ix.w+$)*/
        void op_DDCB_0x36() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SLL (cpu.ix.w+$)*/
        void op_DDCB_0x37() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SLL(ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x38() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x39() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x3a() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x3b() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x3c() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x3d() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SRL (cpu.ix.w+$)*/
        void op_DDCB_0x3e() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SRL (cpu.ix.w+$)*/
        void op_DDCB_0x3f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SRL(ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*BIT 0,(cpu.ix.w+$)*/
        void op_DDCB_0x47() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(0, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 1,(cpu.ix.w+$)*/
        void op_DDCB_0x4f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(1, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 2,(cpu.ix.w+$)*/
        void op_DDCB_0x57() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(2, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 3,(cpu.ix.w+$)*/
        void op_DDCB_0x5f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(3, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 4,(cpu.ix.w+$)*/
        void op_DDCB_0x67() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(4, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 5,(cpu.ix.w+$)*/
        void op_DDCB_0x6f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(5, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 6,(cpu.ix.w+$)*/
        void op_DDCB_0x77() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(6, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*BIT 7,(cpu.ix.w+$)*/
        void op_DDCB_0x7f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            BIT_MPTR(7, cpu.tmpbyte);
            T_WAIT_UNTIL(16);
        }

        /*LD cpu.bc.b,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x80() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x81() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x82() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x83() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x84() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x85() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x86() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 0,(cpu.ix.w+$)*/
        void op_DDCB_0x87() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(0, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x88() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x89() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x8a() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x8b() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x8c() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x8d() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x8e() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 1,(cpu.ix.w+$)*/
        void op_DDCB_0x8f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(1, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x90() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x91() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x92() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x93() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x94() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x95() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x96() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 2,(cpu.ix.w+$)*/
        void op_DDCB_0x97() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(2, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x98() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x99() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x9a() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x9b() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x9c() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x9d() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x9e() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 3,(cpu.ix.w+$)*/
        void op_DDCB_0x9f() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(3, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa0() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa1() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa2() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa3() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa4() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa5() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa6() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 4,(cpu.ix.w+$)*/
        void op_DDCB_0xa7() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(4, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xa8() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xa9() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xaa() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xab() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xac() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xad() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xae() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 5,(cpu.ix.w+$)*/
        void op_DDCB_0xaf() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(5, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb0() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb1() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb2() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb3() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb4() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb5() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb6() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 6,(cpu.ix.w+$)*/
        void op_DDCB_0xb7() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(6, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xb8() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xb9() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xba() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xbb() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xbc() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xbd() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xbe() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,RES 7,(cpu.ix.w+$)*/
        void op_DDCB_0xbf() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            RES(7, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc0() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc1() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc2() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc3() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc4() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc5() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc6() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 0,(cpu.ix.w+$)*/
        void op_DDCB_0xc7() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(0, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xc8() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xc9() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xca() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xcb() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xcc() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xcd() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xce() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 1,(cpu.ix.w+$)*/
        void op_DDCB_0xcf() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(1, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd0() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd1() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd2() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd3() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd4() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd5() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd6() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 2,(cpu.ix.w+$)*/
        void op_DDCB_0xd7() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(2, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xd8() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xd9() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xda() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xdb() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xdc() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xdd() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xde() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 3,(cpu.ix.w+$)*/
        void op_DDCB_0xdf() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(3, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe0() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe1() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe2() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe3() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe4() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe5() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe6() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 4,(cpu.ix.w+$)*/
        void op_DDCB_0xe7() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(4, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xe8() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xe9() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xea() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xeb() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xec() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xed() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xee() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 5,(cpu.ix.w+$)*/
        void op_DDCB_0xef() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(5, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf0() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf1() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf2() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf3() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf4() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf5() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf6() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 6,(cpu.ix.w+$)*/
        void op_DDCB_0xf7() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(6, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.b,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xf8() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.bc.c,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xf9() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.d,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xfa() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.de.d, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.de.e,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xfb() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.de.e, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.h,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xfc() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.hl.l,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xfd() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xfe() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

        /*LD cpu.af.a,SET 7,(cpu.ix.w+$)*/
        void op_DDCB_0xff() {
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SET(7, ref cpu.tmpbyte);
            LD(ref cpu.af.a, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }

    }
}
