﻿namespace Z80Lib {
    partial class Z80 {
        OperationDelegate[] opcodes_cb;

        void init_opcodes_cb() {
            opcodes_cb = new OperationDelegate[] {
                op_CB_0x00, op_CB_0x01, op_CB_0x02, op_CB_0x03,
                op_CB_0x04, op_CB_0x05, op_CB_0x06, op_CB_0x07,
                op_CB_0x08, op_CB_0x09, op_CB_0x0a, op_CB_0x0b,
                op_CB_0x0c, op_CB_0x0d, op_CB_0x0e, op_CB_0x0f,
                op_CB_0x10, op_CB_0x11, op_CB_0x12, op_CB_0x13,
                op_CB_0x14, op_CB_0x15, op_CB_0x16, op_CB_0x17,
                op_CB_0x18, op_CB_0x19, op_CB_0x1a, op_CB_0x1b,
                op_CB_0x1c, op_CB_0x1d, op_CB_0x1e, op_CB_0x1f,
                op_CB_0x20, op_CB_0x21, op_CB_0x22, op_CB_0x23,
                op_CB_0x24, op_CB_0x25, op_CB_0x26, op_CB_0x27,
                op_CB_0x28, op_CB_0x29, op_CB_0x2a, op_CB_0x2b,
                op_CB_0x2c, op_CB_0x2d, op_CB_0x2e, op_CB_0x2f,
                op_CB_0x30, op_CB_0x31, op_CB_0x32, op_CB_0x33,
                op_CB_0x34, op_CB_0x35, op_CB_0x36, op_CB_0x37,
                op_CB_0x38, op_CB_0x39, op_CB_0x3a, op_CB_0x3b,
                op_CB_0x3c, op_CB_0x3d, op_CB_0x3e, op_CB_0x3f,
                op_CB_0x40, op_CB_0x41, op_CB_0x42, op_CB_0x43,
                op_CB_0x44, op_CB_0x45, op_CB_0x46, op_CB_0x47,
                op_CB_0x48, op_CB_0x49, op_CB_0x4a, op_CB_0x4b,
                op_CB_0x4c, op_CB_0x4d, op_CB_0x4e, op_CB_0x4f,
                op_CB_0x50, op_CB_0x51, op_CB_0x52, op_CB_0x53,
                op_CB_0x54, op_CB_0x55, op_CB_0x56, op_CB_0x57,
                op_CB_0x58, op_CB_0x59, op_CB_0x5a, op_CB_0x5b,
                op_CB_0x5c, op_CB_0x5d, op_CB_0x5e, op_CB_0x5f,
                op_CB_0x60, op_CB_0x61, op_CB_0x62, op_CB_0x63,
                op_CB_0x64, op_CB_0x65, op_CB_0x66, op_CB_0x67,
                op_CB_0x68, op_CB_0x69, op_CB_0x6a, op_CB_0x6b,
                op_CB_0x6c, op_CB_0x6d, op_CB_0x6e, op_CB_0x6f,
                op_CB_0x70, op_CB_0x71, op_CB_0x72, op_CB_0x73,
                op_CB_0x74, op_CB_0x75, op_CB_0x76, op_CB_0x77,
                op_CB_0x78, op_CB_0x79, op_CB_0x7a, op_CB_0x7b,
                op_CB_0x7c, op_CB_0x7d, op_CB_0x7e, op_CB_0x7f,
                op_CB_0x80, op_CB_0x81, op_CB_0x82, op_CB_0x83,
                op_CB_0x84, op_CB_0x85, op_CB_0x86, op_CB_0x87,
                op_CB_0x88, op_CB_0x89, op_CB_0x8a, op_CB_0x8b,
                op_CB_0x8c, op_CB_0x8d, op_CB_0x8e, op_CB_0x8f,
                op_CB_0x90, op_CB_0x91, op_CB_0x92, op_CB_0x93,
                op_CB_0x94, op_CB_0x95, op_CB_0x96, op_CB_0x97,
                op_CB_0x98, op_CB_0x99, op_CB_0x9a, op_CB_0x9b,
                op_CB_0x9c, op_CB_0x9d, op_CB_0x9e, op_CB_0x9f,
                op_CB_0xa0, op_CB_0xa1, op_CB_0xa2, op_CB_0xa3,
                op_CB_0xa4, op_CB_0xa5, op_CB_0xa6, op_CB_0xa7,
                op_CB_0xa8, op_CB_0xa9, op_CB_0xaa, op_CB_0xab,
                op_CB_0xac, op_CB_0xad, op_CB_0xae, op_CB_0xaf,
                op_CB_0xb0, op_CB_0xb1, op_CB_0xb2, op_CB_0xb3,
                op_CB_0xb4, op_CB_0xb5, op_CB_0xb6, op_CB_0xb7,
                op_CB_0xb8, op_CB_0xb9, op_CB_0xba, op_CB_0xbb,
                op_CB_0xbc, op_CB_0xbd, op_CB_0xbe, op_CB_0xbf,
                op_CB_0xc0, op_CB_0xc1, op_CB_0xc2, op_CB_0xc3,
                op_CB_0xc4, op_CB_0xc5, op_CB_0xc6, op_CB_0xc7,
                op_CB_0xc8, op_CB_0xc9, op_CB_0xca, op_CB_0xcb,
                op_CB_0xcc, op_CB_0xcd, op_CB_0xce, op_CB_0xcf,
                op_CB_0xd0, op_CB_0xd1, op_CB_0xd2, op_CB_0xd3,
                op_CB_0xd4, op_CB_0xd5, op_CB_0xd6, op_CB_0xd7,
                op_CB_0xd8, op_CB_0xd9, op_CB_0xda, op_CB_0xdb,
                op_CB_0xdc, op_CB_0xdd, op_CB_0xde, op_CB_0xdf,
                op_CB_0xe0, op_CB_0xe1, op_CB_0xe2, op_CB_0xe3,
                op_CB_0xe4, op_CB_0xe5, op_CB_0xe6, op_CB_0xe7,
                op_CB_0xe8, op_CB_0xe9, op_CB_0xea, op_CB_0xeb,
                op_CB_0xec, op_CB_0xed, op_CB_0xee, op_CB_0xef,
                op_CB_0xf0, op_CB_0xf1, op_CB_0xf2, op_CB_0xf3,
                op_CB_0xf4, op_CB_0xf5, op_CB_0xf6, op_CB_0xf7,
                op_CB_0xf8, op_CB_0xf9, op_CB_0xfa, op_CB_0xfb,
                op_CB_0xfc, op_CB_0xfd, op_CB_0xfe, op_CB_0xff
            };
        }

        /*RLC cpu.bc.b*/
        void op_CB_0x00() {
            RLC(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RLC cpu.bc.c*/
        void op_CB_0x01() {
            RLC(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RLC cpu.de.d*/
        void op_CB_0x02() {
            RLC(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RLC cpu.de.e*/
        void op_CB_0x03() {
            RLC(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RLC cpu.hl.h*/
        void op_CB_0x04() {
            RLC(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RLC cpu.hl.l*/
        void op_CB_0x05() {
            RLC(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RLC (cpu.hl.w)*/
        void op_CB_0x06() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RLC(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RLC cpu.af.a*/
        void op_CB_0x07() {
            RLC(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RRC cpu.bc.b*/
        void op_CB_0x08() {
            RRC(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RRC cpu.bc.c*/
        void op_CB_0x09() {
            RRC(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RRC cpu.de.d*/
        void op_CB_0x0a() {
            RRC(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RRC cpu.de.e*/
        void op_CB_0x0b() {
            RRC(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RRC cpu.hl.h*/
        void op_CB_0x0c() {
            RRC(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RRC cpu.hl.l*/
        void op_CB_0x0d() {
            RRC(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RRC (cpu.hl.w)*/
        void op_CB_0x0e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RRC(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RRC cpu.af.a*/
        void op_CB_0x0f() {
            RRC(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RL cpu.bc.b*/
        void op_CB_0x10() {
            RL(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RL cpu.bc.c*/
        void op_CB_0x11() {
            RL(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RL cpu.de.d*/
        void op_CB_0x12() {
            RL(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RL cpu.de.e*/
        void op_CB_0x13() {
            RL(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RL cpu.hl.h*/
        void op_CB_0x14() {
            RL(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RL cpu.hl.l*/
        void op_CB_0x15() {
            RL(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RL (cpu.hl.w)*/
        void op_CB_0x16() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RL(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RL cpu.af.a*/
        void op_CB_0x17() {
            RL(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RR cpu.bc.b*/
        void op_CB_0x18() {
            RR(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RR cpu.bc.c*/
        void op_CB_0x19() {
            RR(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RR cpu.de.d*/
        void op_CB_0x1a() {
            RR(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RR cpu.de.e*/
        void op_CB_0x1b() {
            RR(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RR cpu.hl.h*/
        void op_CB_0x1c() {
            RR(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RR cpu.hl.l*/
        void op_CB_0x1d() {
            RR(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RR (cpu.hl.w)*/
        void op_CB_0x1e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RR(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RR cpu.af.a*/
        void op_CB_0x1f() {
            RR(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SLA cpu.bc.b*/
        void op_CB_0x20() {
            SLA(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SLA cpu.bc.c*/
        void op_CB_0x21() {
            SLA(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SLA cpu.de.d*/
        void op_CB_0x22() {
            SLA(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SLA cpu.de.e*/
        void op_CB_0x23() {
            SLA(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SLA cpu.hl.h*/
        void op_CB_0x24() {
            SLA(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SLA cpu.hl.l*/
        void op_CB_0x25() {
            SLA(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SLA (cpu.hl.w)*/
        void op_CB_0x26() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SLA(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SLA cpu.af.a*/
        void op_CB_0x27() {
            SLA(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SRA cpu.bc.b*/
        void op_CB_0x28() {
            SRA(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SRA cpu.bc.c*/
        void op_CB_0x29() {
            SRA(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SRA cpu.de.d*/
        void op_CB_0x2a() {
            SRA(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SRA cpu.de.e*/
        void op_CB_0x2b() {
            SRA(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SRA cpu.hl.h*/
        void op_CB_0x2c() {
            SRA(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SRA cpu.hl.l*/
        void op_CB_0x2d() {
            SRA(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SRA (cpu.hl.w)*/
        void op_CB_0x2e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SRA(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SRA cpu.af.a*/
        void op_CB_0x2f() {
            SRA(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SLL cpu.bc.b*/
        void op_CB_0x30() {
            SLL(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SLL cpu.bc.c*/
        void op_CB_0x31() {
            SLL(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SLL cpu.de.d*/
        void op_CB_0x32() {
            SLL(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SLL cpu.de.e*/
        void op_CB_0x33() {
            SLL(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SLL cpu.hl.h*/
        void op_CB_0x34() {
            SLL(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SLL cpu.hl.l*/
        void op_CB_0x35() {
            SLL(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SLL (cpu.hl.w)*/
        void op_CB_0x36() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SLL(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SLL cpu.af.a*/
        void op_CB_0x37() {
            SLL(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SRL cpu.bc.b*/
        void op_CB_0x38() {
            SRL(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SRL cpu.bc.c*/
        void op_CB_0x39() {
            SRL(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SRL cpu.de.d*/
        void op_CB_0x3a() {
            SRL(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SRL cpu.de.e*/
        void op_CB_0x3b() {
            SRL(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SRL cpu.hl.h*/
        void op_CB_0x3c() {
            SRL(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SRL cpu.hl.l*/
        void op_CB_0x3d() {
            SRL(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SRL (cpu.hl.w)*/
        void op_CB_0x3e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SRL(ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SRL cpu.af.a*/
        void op_CB_0x3f() {
            SRL(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,cpu.bc.b*/
        void op_CB_0x40() {
            BIT(0, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,cpu.bc.c*/
        void op_CB_0x41() {
            BIT(0, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,cpu.de.d*/
        void op_CB_0x42() {
            BIT(0, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,cpu.de.e*/
        void op_CB_0x43() {
            BIT(0, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,cpu.hl.h*/
        void op_CB_0x44() {
            BIT(0, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,cpu.hl.l*/
        void op_CB_0x45() {
            BIT(0, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 0,(cpu.hl.w)*/
        void op_CB_0x46() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT_MPTR(0, cpu.tmpbyte);
            T_WAIT_UNTIL(8);
        }
        /*BIT 0,cpu.af.a*/
        void op_CB_0x47() {
            BIT(0, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,cpu.bc.b*/
        void op_CB_0x48() {
            BIT(1, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,cpu.bc.c*/
        void op_CB_0x49() {
            BIT(1, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,cpu.de.d*/
        void op_CB_0x4a() {
            BIT(1, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,cpu.de.e*/
        void op_CB_0x4b() {
            BIT(1, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,cpu.hl.h*/
        void op_CB_0x4c() {
            BIT(1, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,cpu.hl.l*/
        void op_CB_0x4d() {
            BIT(1, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 1,(cpu.hl.w)*/
        void op_CB_0x4e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT(1, cpu.tmpbyte); // BIT_MPTR replaced based on fuse test suite
            T_WAIT_UNTIL(8);
        }
        /*BIT 1,cpu.af.a*/
        void op_CB_0x4f() {
            BIT(1, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,cpu.bc.b*/
        void op_CB_0x50() {
            BIT(2, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,cpu.bc.c*/
        void op_CB_0x51() {
            BIT(2, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,cpu.de.d*/
        void op_CB_0x52() {
            BIT(2, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,cpu.de.e*/
        void op_CB_0x53() {
            BIT(2, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,cpu.hl.h*/
        void op_CB_0x54() {
            BIT(2, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,cpu.hl.l*/
        void op_CB_0x55() {
            BIT(2, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 2,(cpu.hl.w)*/
        void op_CB_0x56() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT_MPTR(2, cpu.tmpbyte);
            T_WAIT_UNTIL(8);
        }
        /*BIT 2,cpu.af.a*/
        void op_CB_0x57() {
            BIT(2, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,cpu.bc.b*/
        void op_CB_0x58() {
            BIT(3, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,cpu.bc.c*/
        void op_CB_0x59() {
            BIT(3, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,cpu.de.d*/
        void op_CB_0x5a() {
            BIT(3, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,cpu.de.e*/
        void op_CB_0x5b() {
            BIT(3, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,cpu.hl.h*/
        void op_CB_0x5c() {
            BIT(3, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,cpu.hl.l*/
        void op_CB_0x5d() {
            BIT(3, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 3,(cpu.hl.w)*/
        void op_CB_0x5e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT(3, cpu.tmpbyte); // BIT_MPTR replaced based on fuse test suite
            T_WAIT_UNTIL(8);
        }
        /*BIT 3,cpu.af.a*/
        void op_CB_0x5f() {
            BIT(3, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,cpu.bc.b*/
        void op_CB_0x60() {
            BIT(4, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,cpu.bc.c*/
        void op_CB_0x61() {
            BIT(4, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,cpu.de.d*/
        void op_CB_0x62() {
            BIT(4, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,cpu.de.e*/
        void op_CB_0x63() {
            BIT(4, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,cpu.hl.h*/
        void op_CB_0x64() {
            BIT(4, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,cpu.hl.l*/
        void op_CB_0x65() {
            BIT(4, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 4,(cpu.hl.w)*/
        void op_CB_0x66() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT_MPTR(4, cpu.tmpbyte);
            T_WAIT_UNTIL(8);
        }
        /*BIT 4,cpu.af.a*/
        void op_CB_0x67() {
            BIT(4, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,cpu.bc.b*/
        void op_CB_0x68() {
            BIT(5, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,cpu.bc.c*/
        void op_CB_0x69() {
            BIT(5, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,cpu.de.d*/
        void op_CB_0x6a() {
            BIT(5, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,cpu.de.e*/
        void op_CB_0x6b() {
            BIT(5, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,cpu.hl.h*/
        void op_CB_0x6c() {
            BIT(5, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,cpu.hl.l*/
        void op_CB_0x6d() {
            BIT(5, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 5,(cpu.hl.w)*/
        void op_CB_0x6e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT(5, cpu.tmpbyte); // BIT_MPTR replaced based on fuse test suite
            T_WAIT_UNTIL(8);
        }
        /*BIT 5,cpu.af.a*/
        void op_CB_0x6f() {
            BIT(5, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,cpu.bc.b*/
        void op_CB_0x70() {
            BIT(6, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,cpu.bc.c*/
        void op_CB_0x71() {
            BIT(6, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,cpu.de.d*/
        void op_CB_0x72() {
            BIT(6, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,cpu.de.e*/
        void op_CB_0x73() {
            BIT(6, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,cpu.hl.h*/
        void op_CB_0x74() {
            BIT(6, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,cpu.hl.l*/
        void op_CB_0x75() {
            BIT(6, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 6,(cpu.hl.w)*/
        void op_CB_0x76() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT(6, cpu.tmpbyte); // BIT_MPTR replaced based on fuse test suite
            T_WAIT_UNTIL(8);
        }
        /*BIT 6,cpu.af.a*/
        void op_CB_0x77() {
            BIT(6, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,cpu.bc.b*/
        void op_CB_0x78() {
            BIT(7, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,cpu.bc.c*/
        void op_CB_0x79() {
            BIT(7, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,cpu.de.d*/
        void op_CB_0x7a() {
            BIT(7, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,cpu.de.e*/
        void op_CB_0x7b() {
            BIT(7, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,cpu.hl.h*/
        void op_CB_0x7c() {
            BIT(7, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,cpu.hl.l*/
        void op_CB_0x7d() {
            BIT(7, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*BIT 7,(cpu.hl.w)*/
        void op_CB_0x7e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            BIT_MPTR(7, cpu.tmpbyte);
            T_WAIT_UNTIL(8);
        }
        /*BIT 7,cpu.af.a*/
        void op_CB_0x7f() {
            BIT(7, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,cpu.bc.b*/
        void op_CB_0x80() {
            RES(0, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,cpu.bc.c*/
        void op_CB_0x81() {
            RES(0, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,cpu.de.d*/
        void op_CB_0x82() {
            RES(0, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,cpu.de.e*/
        void op_CB_0x83() {
            RES(0, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,cpu.hl.h*/
        void op_CB_0x84() {
            RES(0, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,cpu.hl.l*/
        void op_CB_0x85() {
            RES(0, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 0,(cpu.hl.w)*/
        void op_CB_0x86() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(0, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 0,cpu.af.a*/
        void op_CB_0x87() {
            RES(0, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,cpu.bc.b*/
        void op_CB_0x88() {
            RES(1, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,cpu.bc.c*/
        void op_CB_0x89() {
            RES(1, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,cpu.de.d*/
        void op_CB_0x8a() {
            RES(1, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,cpu.de.e*/
        void op_CB_0x8b() {
            RES(1, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,cpu.hl.h*/
        void op_CB_0x8c() {
            RES(1, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,cpu.hl.l*/
        void op_CB_0x8d() {
            RES(1, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 1,(cpu.hl.w)*/
        void op_CB_0x8e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(1, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 1,cpu.af.a*/
        void op_CB_0x8f() {
            RES(1, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,cpu.bc.b*/
        void op_CB_0x90() {
            RES(2, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,cpu.bc.c*/
        void op_CB_0x91() {
            RES(2, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,cpu.de.d*/
        void op_CB_0x92() {
            RES(2, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,cpu.de.e*/
        void op_CB_0x93() {
            RES(2, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,cpu.hl.h*/
        void op_CB_0x94() {
            RES(2, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,cpu.hl.l*/
        void op_CB_0x95() {
            RES(2, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 2,(cpu.hl.w)*/
        void op_CB_0x96() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(2, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 2,cpu.af.a*/
        void op_CB_0x97() {
            RES(2, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,cpu.bc.b*/
        void op_CB_0x98() {
            RES(3, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,cpu.bc.c*/
        void op_CB_0x99() {
            RES(3, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,cpu.de.d*/
        void op_CB_0x9a() {
            RES(3, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,cpu.de.e*/
        void op_CB_0x9b() {
            RES(3, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,cpu.hl.h*/
        void op_CB_0x9c() {
            RES(3, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,cpu.hl.l*/
        void op_CB_0x9d() {
            RES(3, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 3,(cpu.hl.w)*/
        void op_CB_0x9e() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(3, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 3,cpu.af.a*/
        void op_CB_0x9f() {
            RES(3, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,cpu.bc.b*/
        void op_CB_0xa0() {
            RES(4, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,cpu.bc.c*/
        void op_CB_0xa1() {
            RES(4, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,cpu.de.d*/
        void op_CB_0xa2() {
            RES(4, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,cpu.de.e*/
        void op_CB_0xa3() {
            RES(4, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,cpu.hl.h*/
        void op_CB_0xa4() {
            RES(4, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,cpu.hl.l*/
        void op_CB_0xa5() {
            RES(4, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 4,(cpu.hl.w)*/
        void op_CB_0xa6() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(4, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 4,cpu.af.a*/
        void op_CB_0xa7() {
            RES(4, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,cpu.bc.b*/
        void op_CB_0xa8() {
            RES(5, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,cpu.bc.c*/
        void op_CB_0xa9() {
            RES(5, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,cpu.de.d*/
        void op_CB_0xaa() {
            RES(5, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,cpu.de.e*/
        void op_CB_0xab() {
            RES(5, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,cpu.hl.h*/
        void op_CB_0xac() {
            RES(5, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,cpu.hl.l*/
        void op_CB_0xad() {
            RES(5, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 5,(cpu.hl.w)*/
        void op_CB_0xae() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(5, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 5,cpu.af.a*/
        void op_CB_0xaf() {
            RES(5, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,cpu.bc.b*/
        void op_CB_0xb0() {
            RES(6, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,cpu.bc.c*/
        void op_CB_0xb1() {
            RES(6, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,cpu.de.d*/
        void op_CB_0xb2() {
            RES(6, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,cpu.de.e*/
        void op_CB_0xb3() {
            RES(6, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,cpu.hl.h*/
        void op_CB_0xb4() {
            RES(6, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,cpu.hl.l*/
        void op_CB_0xb5() {
            RES(6, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 6,(cpu.hl.w)*/
        void op_CB_0xb6() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(6, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 6,cpu.af.a*/
        void op_CB_0xb7() {
            RES(6, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,cpu.bc.b*/
        void op_CB_0xb8() {
            RES(7, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,cpu.bc.c*/
        void op_CB_0xb9() {
            RES(7, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,cpu.de.d*/
        void op_CB_0xba() {
            RES(7, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,cpu.de.e*/
        void op_CB_0xbb() {
            RES(7, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,cpu.hl.h*/
        void op_CB_0xbc() {
            RES(7, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,cpu.hl.l*/
        void op_CB_0xbd() {
            RES(7, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*RES 7,(cpu.hl.w)*/
        void op_CB_0xbe() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            RES(7, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*RES 7,cpu.af.a*/
        void op_CB_0xbf() {
            RES(7, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,cpu.bc.b*/
        void op_CB_0xc0() {
            SET(0, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,cpu.bc.c*/
        void op_CB_0xc1() {
            SET(0, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,cpu.de.d*/
        void op_CB_0xc2() {
            SET(0, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,cpu.de.e*/
        void op_CB_0xc3() {
            SET(0, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,cpu.hl.h*/
        void op_CB_0xc4() {
            SET(0, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,cpu.hl.l*/
        void op_CB_0xc5() {
            SET(0, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 0,(cpu.hl.w)*/
        void op_CB_0xc6() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(0, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 0,cpu.af.a*/
        void op_CB_0xc7() {
            SET(0, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,cpu.bc.b*/
        void op_CB_0xc8() {
            SET(1, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,cpu.bc.c*/
        void op_CB_0xc9() {
            SET(1, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,cpu.de.d*/
        void op_CB_0xca() {
            SET(1, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,cpu.de.e*/
        void op_CB_0xcb() {
            SET(1, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,cpu.hl.h*/
        void op_CB_0xcc() {
            SET(1, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,cpu.hl.l*/
        void op_CB_0xcd() {
            SET(1, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 1,(cpu.hl.w)*/
        void op_CB_0xce() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(1, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 1,cpu.af.a*/
        void op_CB_0xcf() {
            SET(1, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,cpu.bc.b*/
        void op_CB_0xd0() {
            SET(2, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,cpu.bc.c*/
        void op_CB_0xd1() {
            SET(2, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,cpu.de.d*/
        void op_CB_0xd2() {
            SET(2, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,cpu.de.e*/
        void op_CB_0xd3() {
            SET(2, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,cpu.hl.h*/
        void op_CB_0xd4() {
            SET(2, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,cpu.hl.l*/
        void op_CB_0xd5() {
            SET(2, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 2,(cpu.hl.w)*/
        void op_CB_0xd6() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(2, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 2,cpu.af.a*/
        void op_CB_0xd7() {
            SET(2, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,cpu.bc.b*/
        void op_CB_0xd8() {
            SET(3, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,cpu.bc.c*/
        void op_CB_0xd9() {
            SET(3, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,cpu.de.d*/
        void op_CB_0xda() {
            SET(3, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,cpu.de.e*/
        void op_CB_0xdb() {
            SET(3, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,cpu.hl.h*/
        void op_CB_0xdc() {
            SET(3, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,cpu.hl.l*/
        void op_CB_0xdd() {
            SET(3, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 3,(cpu.hl.w)*/
        void op_CB_0xde() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(3, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 3,cpu.af.a*/
        void op_CB_0xdf() {
            SET(3, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,cpu.bc.b*/
        void op_CB_0xe0() {
            SET(4, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,cpu.bc.c*/
        void op_CB_0xe1() {
            SET(4, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,cpu.de.d*/
        void op_CB_0xe2() {
            SET(4, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,cpu.de.e*/
        void op_CB_0xe3() {
            SET(4, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,cpu.hl.h*/
        void op_CB_0xe4() {
            SET(4, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,cpu.hl.l*/
        void op_CB_0xe5() {
            SET(4, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 4,(cpu.hl.w)*/
        void op_CB_0xe6() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(4, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 4,cpu.af.a*/
        void op_CB_0xe7() {
            SET(4, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,cpu.bc.b*/
        void op_CB_0xe8() {
            SET(5, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,cpu.bc.c*/
        void op_CB_0xe9() {
            SET(5, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,cpu.de.d*/
        void op_CB_0xea() {
            SET(5, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,cpu.de.e*/
        void op_CB_0xeb() {
            SET(5, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,cpu.hl.h*/
        void op_CB_0xec() {
            SET(5, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,cpu.hl.l*/
        void op_CB_0xed() {
            SET(5, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 5,(cpu.hl.w)*/
        void op_CB_0xee() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(5, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 5,cpu.af.a*/
        void op_CB_0xef() {
            SET(5, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,cpu.bc.b*/
        void op_CB_0xf0() {
            SET(6, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,cpu.bc.c*/
        void op_CB_0xf1() {
            SET(6, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,cpu.de.d*/
        void op_CB_0xf2() {
            SET(6, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,cpu.de.e*/
        void op_CB_0xf3() {
            SET(6, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,cpu.hl.h*/
        void op_CB_0xf4() {
            SET(6, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,cpu.hl.l*/
        void op_CB_0xf5() {
            SET(6, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 6,(cpu.hl.w)*/
        void op_CB_0xf6() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(6, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 6,cpu.af.a*/
        void op_CB_0xf7() {
            SET(6, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,cpu.bc.b*/
        void op_CB_0xf8() {
            SET(7, ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,cpu.bc.c*/
        void op_CB_0xf9() {
            SET(7, ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,cpu.de.d*/
        void op_CB_0xfa() {
            SET(7, ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,cpu.de.e*/
        void op_CB_0xfb() {
            SET(7, ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,cpu.hl.h*/
        void op_CB_0xfc() {
            SET(7, ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,cpu.hl.l*/
        void op_CB_0xfd() {
            SET(7, ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }
        /*SET 7,(cpu.hl.w)*/
        void op_CB_0xfe() {
            cpu.tmpbyte = READ_MEM((cpu.hl.w), 4);
            SET(7, ref cpu.tmpbyte);
            WRITE_MEM((cpu.hl.w), cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }
        /*SET 7,cpu.af.a*/
        void op_CB_0xff() {
            SET(7, ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }

    }
}
