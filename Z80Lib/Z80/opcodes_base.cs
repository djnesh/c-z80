﻿namespace Z80Lib {
    partial class Z80 {
        OperationDelegate[] opcodes_base;

        void init_opcodes_base() {
            opcodes_base = new OperationDelegate[] {
                 op_0x00, op_0x01, op_0x02, op_0x03,
                 op_0x04, op_0x05, op_0x06, op_0x07,
                 op_0x08, op_0x09, op_0x0a, op_0x0b,
                 op_0x0c, op_0x0d, op_0x0e, op_0x0f,
                 op_0x10, op_0x11, op_0x12, op_0x13,
                 op_0x14, op_0x15, op_0x16, op_0x17,
                 op_0x18, op_0x19, op_0x1a, op_0x1b,
                 op_0x1c, op_0x1d, op_0x1e, op_0x1f,
                 op_0x20, op_0x21, op_0x22, op_0x23,
                 op_0x24, op_0x25, op_0x26, op_0x27,
                 op_0x28, op_0x29, op_0x2a, op_0x2b,
                 op_0x2c, op_0x2d, op_0x2e, op_0x2f,
                 op_0x30, op_0x31, op_0x32, op_0x33,
                 op_0x34, op_0x35, op_0x36, op_0x37,
                 op_0x38, op_0x39, op_0x3a, op_0x3b,
                 op_0x3c, op_0x3d, op_0x3e, op_0x3f,
                 op_0x40, op_0x41, op_0x42, op_0x43,
                 op_0x44, op_0x45, op_0x46, op_0x47,
                 op_0x48, op_0x49, op_0x4a, op_0x4b,
                 op_0x4c, op_0x4d, op_0x4e, op_0x4f,
                 op_0x50, op_0x51, op_0x52, op_0x53,
                 op_0x54, op_0x55, op_0x56, op_0x57,
                 op_0x58, op_0x59, op_0x5a, op_0x5b,
                 op_0x5c, op_0x5d, op_0x5e, op_0x5f,
                 op_0x60, op_0x61, op_0x62, op_0x63,
                 op_0x64, op_0x65, op_0x66, op_0x67,
                 op_0x68, op_0x69, op_0x6a, op_0x6b,
                 op_0x6c, op_0x6d, op_0x6e, op_0x6f,
                 op_0x70, op_0x71, op_0x72, op_0x73,
                 op_0x74, op_0x75, op_0x76, op_0x77,
                 op_0x78, op_0x79, op_0x7a, op_0x7b,
                 op_0x7c, op_0x7d, op_0x7e, op_0x7f,
                 op_0x80, op_0x81, op_0x82, op_0x83,
                 op_0x84, op_0x85, op_0x86, op_0x87,
                 op_0x88, op_0x89, op_0x8a, op_0x8b,
                 op_0x8c, op_0x8d, op_0x8e, op_0x8f,
                 op_0x90, op_0x91, op_0x92, op_0x93,
                 op_0x94, op_0x95, op_0x96, op_0x97,
                 op_0x98, op_0x99, op_0x9a, op_0x9b,
                 op_0x9c, op_0x9d, op_0x9e, op_0x9f,
                 op_0xa0, op_0xa1, op_0xa2, op_0xa3,
                 op_0xa4, op_0xa5, op_0xa6, op_0xa7,
                 op_0xa8, op_0xa9, op_0xaa, op_0xab,
                 op_0xac, op_0xad, op_0xae, op_0xaf,
                 op_0xb0, op_0xb1, op_0xb2, op_0xb3,
                 op_0xb4, op_0xb5, op_0xb6, op_0xb7,
                 op_0xb8, op_0xb9, op_0xba, op_0xbb,
                 op_0xbc, op_0xbd, op_0xbe, op_0xbf,
                 op_0xc0, op_0xc1, op_0xc2, op_0xc3,
                 op_0xc4, op_0xc5, op_0xc6, op_0xc7,
                 op_0xc8, op_0xc9, op_0xca, op_p_CB,
                 op_0xcc, op_0xcd, op_0xce, op_0xcf,
                 op_0xd0, op_0xd1, op_0xd2, op_0xd3,
                 op_0xd4, op_0xd5, op_0xd6, op_0xd7,
                 op_0xd8, op_0xd9, op_0xda, op_0xdb,
                 op_0xdc, op_p_DD, op_0xde, op_0xdf,
                 op_0xe0, op_0xe1, op_0xe2, op_0xe3,
                 op_0xe4, op_0xe5, op_0xe6, op_0xe7,
                 op_0xe8, op_0xe9, op_0xea, op_0xeb,
                 op_0xec, op_p_ED, op_0xee, op_0xef,
                 op_0xf0, op_0xf1, op_0xf2, op_0xf3,
                 op_0xf4, op_0xf5, op_0xf6, op_0xf7,
                 op_0xf8, op_0xf9, op_0xfa, op_0xfb,
                 op_0xfc, op_p_FD, op_0xfe, op_0xff
            };
        }

        /*NOP*/
        void op_0x00() {
            T_WAIT_UNTIL(4);
        }

        /*LD BC,@*/
        void op_0x01() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            LD16(ref cpu.bc, cpu.tmpword);
            T_WAIT_UNTIL(10);
        }

        /*LD (BC),cpu.af.a*/
        void op_0x02() {
            LD_A_TO_ADDR_MPTR(ref cpu.tmpbyte, cpu.af.a, cpu.bc.w);
            WRITE_MEM((cpu.bc.w), cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*INC BC*/
        void op_0x03() {
            INC16(ref cpu.bc);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.bc.b*/
        void op_0x04() {
            INC(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.bc.b*/
        void op_0x05() {
            DEC(ref cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b,#*/
        void op_0x06() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.bc.b, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RLCA*/
        void op_0x07() {
            RLCA();
            T_WAIT_UNTIL(4);
        }

        /*EX cpu.af.w, cpu.af.w'*/
        void op_0x08() {
            EX(ref cpu.af, ref cpu.af_);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.hl.w, BC*/
        void op_0x09() {
            ADD16(ref cpu.hl, cpu.bc);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.af.a,(BC)*/
        void op_0x0a() {
            cpu.tmpbyte = READ_MEM(cpu.bc.w, 4);
            LD_A_FROM_ADDR_MPTR(ref cpu.af.a, cpu.tmpbyte, cpu.bc.w);
            T_WAIT_UNTIL(7);
        }

        /*DEC BC*/
        void op_0x0b() {
            DEC16(ref cpu.bc);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.bc.c*/
        void op_0x0c() {
            INC(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.bc.c*/
        void op_0x0d() {
            DEC(ref cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c,#*/
        void op_0x0e() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.bc.c, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RRCA*/
        void op_0x0f() {
            RRCA();
            T_WAIT_UNTIL(4);
        }

        /*DJNZ %*/
        void op_0x10() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            DJNZ(cpu.tmpbyte_s, /*t:*/ /*t1*/8,/*t2*/13);
        }

        /*LD cpu.de.w,@*/
        void op_0x11() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            LD16(ref cpu.de, cpu.tmpword);
            T_WAIT_UNTIL(10);
        }

        /*LD (cpu.de.w),cpu.af.a*/
        void op_0x12() {
            LD_A_TO_ADDR_MPTR(ref cpu.tmpbyte, cpu.af.a, cpu.de.w);
            WRITE_MEM((cpu.de.w), cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*INC cpu.de.w*/
        void op_0x13() {
            INC16(ref cpu.de);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.de.d*/
        void op_0x14() {
            INC(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.de.d*/
        void op_0x15() {
            DEC(ref cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d,#*/
        void op_0x16() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.de.d, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RLA*/
        void op_0x17() {
            RLA();
            T_WAIT_UNTIL(4);
        }

        /*JR %*/
        void op_0x18() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            JR(cpu.tmpbyte_s);
            T_WAIT_UNTIL(12);
        }

        /*ADD cpu.hl.w, cpu.de.w*/
        void op_0x19() {
            ADD16(ref cpu.hl, cpu.de);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.af.a,(cpu.de.w)*/
        void op_0x1a() {
            cpu.tmpbyte = READ_MEM(cpu.de.w, 4);
            LD_A_FROM_ADDR_MPTR(ref cpu.af.a, cpu.tmpbyte, cpu.de.w);
            T_WAIT_UNTIL(7);
        }

        /*DEC cpu.de.w*/
        void op_0x1b() {
            DEC16(ref cpu.de);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.de.e*/
        void op_0x1c() {
            INC(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.de.e*/
        void op_0x1d() {
            DEC(ref cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e,#*/
        void op_0x1e() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.de.e, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RRA*/
        void op_0x1f() {
            RRA();
            T_WAIT_UNTIL(4);
        }

        /*JR NZ,%*/
        void op_0x20() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            if((cpu.af.f & Tables.FLAG_Z) == 0) {
                JR(cpu.tmpbyte_s);
                T_WAIT_UNTIL(12);
            } else { T_WAIT_UNTIL(7); }
        }

        /*LD cpu.hl.w,@*/
        void op_0x21() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            LD16(ref cpu.hl, cpu.tmpword);
            T_WAIT_UNTIL(10);
        }

        /*LD (@),cpu.hl.w*/
        void op_0x22() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.hl, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }

        /*INC cpu.hl.w*/
        void op_0x23() {
            INC16(ref cpu.hl);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.hl.h*/
        void op_0x24() {
            INC(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.hl.h*/
        void op_0x25() {
            DEC(ref cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h,#*/
        void op_0x26() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.hl.h, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*DAA*/
        void op_0x27() {
            DAA();
            T_WAIT_UNTIL(4);
        }

        /*JR Z,%*/
        void op_0x28() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            if((cpu.af.f & Tables.FLAG_Z) != 0) {
                JR(cpu.tmpbyte_s);
                T_WAIT_UNTIL(12);
            } else { T_WAIT_UNTIL(7); }
        }

        /*ADD cpu.hl.w, cpu.hl.w*/
        void op_0x29() {
            ADD16(ref cpu.hl, cpu.hl);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.hl.w,(@)*/
        void op_0x2a() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.hl, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }

        /*DEC cpu.hl.w*/
        void op_0x2b() {
            DEC16(ref cpu.hl);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.hl.l*/
        void op_0x2c() {
            INC(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.hl.l*/
        void op_0x2d() {
            DEC(ref cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l,#*/
        void op_0x2e() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.hl.l, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*CPL*/
        void op_0x2f() {
            CPL();
            T_WAIT_UNTIL(4);
        }

        /*JR NC,%*/
        void op_0x30() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            if((cpu.af.f & Tables.FLAG_C) == 0) {
                JR(cpu.tmpbyte_s);
                T_WAIT_UNTIL(12);
            } else { T_WAIT_UNTIL(7); }
        }

        /*LD cpu.sp.w,@*/
        void op_0x31() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            LD16(ref cpu.sp, cpu.tmpword);
            T_WAIT_UNTIL(10);
        }

        /*LD (@),cpu.af.a*/
        void op_0x32() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_A_TO_ADDR_MPTR(ref cpu.tmpbyte, cpu.af.a, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpbyte, 10);
            T_WAIT_UNTIL(13);
        }

        /*INC cpu.sp.w*/
        void op_0x33() {
            INC16(ref cpu.sp);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.hl.w*/
        void op_0x34() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            INC(ref cpu.tmpbyte);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }

        /*DEC cpu.hl.w*/
        void op_0x35() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            DEC(ref cpu.tmpbyte);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 8);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.hl.w,#*/
        void op_0x36() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.tmpbyte, cpu.tmpbyte);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 7);
            T_WAIT_UNTIL(10);
        }

        /*SCF*/
        void op_0x37() {
            SCF();
            T_WAIT_UNTIL(4);
        }

        /*JR cpu.bc.c,%*/
        void op_0x38() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            if((cpu.af.f & Tables.FLAG_C) != 0) {
                JR(cpu.tmpbyte_s);
                T_WAIT_UNTIL(12);
            } else { T_WAIT_UNTIL(7); }
        }

        /*ADD cpu.hl.w, cpu.sp.w*/
        void op_0x39() {
            ADD16(ref cpu.hl, cpu.sp);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.af.a,(@)*/
        void op_0x3a() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpbyte = READ_MEM(cpu.tmpaddr.w, 10);
            LD_A_FROM_ADDR_MPTR(ref cpu.af.a, cpu.tmpbyte, cpu.tmpaddr.w);
            T_WAIT_UNTIL(13);
        }

        /*DEC cpu.sp.w*/
        void op_0x3b() {
            DEC16(ref cpu.sp);
            T_WAIT_UNTIL(6);
        }

        /*INC cpu.af.a*/
        void op_0x3c() {
            INC(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*DEC cpu.af.a*/
        void op_0x3d() {
            DEC(ref cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a,#*/
        void op_0x3e() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.af.a, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*CCF*/
        void op_0x3f() {
            CCF();
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b, cpu.bc.b*/
        void op_0x40() {
            LD(ref cpu.bc.b, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b, cpu.bc.c*/
        void op_0x41() {
            LD(ref cpu.bc.b, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b, cpu.de.d*/
        void op_0x42() {
            LD(ref cpu.bc.b, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b, cpu.de.e*/
        void op_0x43() {
            LD(ref cpu.bc.b, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b, cpu.hl.h*/
        void op_0x44() {
            LD(ref cpu.bc.b, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b, cpu.hl.l*/
        void op_0x45() {
            LD(ref cpu.bc.b, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.b,cpu.hl.w*/
        void op_0x46() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.bc.b, cpu.af.a*/
        void op_0x47() {
            LD(ref cpu.bc.b, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c, cpu.bc.b*/
        void op_0x48() {
            LD(ref cpu.bc.c, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c, cpu.bc.c*/
        void op_0x49() {
            LD(ref cpu.bc.c, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c, cpu.de.d*/
        void op_0x4a() {
            LD(ref cpu.bc.c, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c, cpu.de.e*/
        void op_0x4b() {
            LD(ref cpu.bc.c, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c, cpu.hl.h*/
        void op_0x4c() {
            LD(ref cpu.bc.c, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c, cpu.hl.l*/
        void op_0x4d() {
            LD(ref cpu.bc.c, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.bc.c,cpu.hl.w*/
        void op_0x4e() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.bc.c, cpu.af.a*/
        void op_0x4f() {
            LD(ref cpu.bc.c, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d, cpu.bc.b*/
        void op_0x50() {
            LD(ref cpu.de.d, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d, cpu.bc.c*/
        void op_0x51() {
            LD(ref cpu.de.d, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d, cpu.de.d*/
        void op_0x52() {
            LD(ref cpu.de.d, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d, cpu.de.e*/
        void op_0x53() {
            LD(ref cpu.de.d, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d, cpu.hl.h*/
        void op_0x54() {
            LD(ref cpu.de.d, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d, cpu.hl.l*/
        void op_0x55() {
            LD(ref cpu.de.d, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.d,cpu.hl.w*/
        void op_0x56() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.de.d, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.de.d, cpu.af.a*/
        void op_0x57() {
            LD(ref cpu.de.d, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e, cpu.bc.b*/
        void op_0x58() {
            LD(ref cpu.de.e, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e, cpu.bc.c*/
        void op_0x59() {
            LD(ref cpu.de.e, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e, cpu.de.d*/
        void op_0x5a() {
            LD(ref cpu.de.e, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e, cpu.de.e*/
        void op_0x5b() {
            LD(ref cpu.de.e, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e, cpu.hl.h*/
        void op_0x5c() {
            LD(ref cpu.de.e, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e, cpu.hl.l*/
        void op_0x5d() {
            LD(ref cpu.de.e, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.de.e,cpu.hl.w*/
        void op_0x5e() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.de.e, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.de.e, cpu.af.a*/
        void op_0x5f() {
            LD(ref cpu.de.e, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h, cpu.bc.b*/
        void op_0x60() {
            LD(ref cpu.hl.h, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h, cpu.bc.c*/
        void op_0x61() {
            LD(ref cpu.hl.h, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h, cpu.de.d*/
        void op_0x62() {
            LD(ref cpu.hl.h, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h, cpu.de.e*/
        void op_0x63() {
            LD(ref cpu.hl.h, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h, cpu.hl.h*/
        void op_0x64() {
            LD(ref cpu.hl.h, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h, cpu.hl.l*/
        void op_0x65() {
            LD(ref cpu.hl.h, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.h,cpu.hl.w*/
        void op_0x66() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.h, cpu.af.a*/
        void op_0x67() {
            LD(ref cpu.hl.h, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l, cpu.bc.b*/
        void op_0x68() {
            LD(ref cpu.hl.l, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l, cpu.bc.c*/
        void op_0x69() {
            LD(ref cpu.hl.l, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l, cpu.de.d*/
        void op_0x6a() {
            LD(ref cpu.hl.l, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l, cpu.de.e*/
        void op_0x6b() {
            LD(ref cpu.hl.l, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l, cpu.hl.h*/
        void op_0x6c() {
            LD(ref cpu.hl.l, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l, cpu.hl.l*/
        void op_0x6d() {
            LD(ref cpu.hl.l, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.l,cpu.hl.w*/
        void op_0x6e() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.l, cpu.af.a*/
        void op_0x6f() {
            LD(ref cpu.hl.l, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.w,cpu.bc.b*/
        void op_0x70() {
            LD(ref cpu.tmpbyte, cpu.bc.b);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.w,cpu.bc.c*/
        void op_0x71() {
            LD(ref cpu.tmpbyte, cpu.bc.c);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.w,cpu.de.d*/
        void op_0x72() {
            LD(ref cpu.tmpbyte, cpu.de.d);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.w,cpu.de.e*/
        void op_0x73() {
            LD(ref cpu.tmpbyte, cpu.de.e);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.w,cpu.hl.h*/
        void op_0x74() {
            LD(ref cpu.tmpbyte, cpu.hl.h);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.hl.w,cpu.hl.l*/
        void op_0x75() {
            LD(ref cpu.tmpbyte, cpu.hl.l);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*HALT*/
        void op_0x76() {
            HALT();
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.hl.w,cpu.af.a*/
        void op_0x77() {
            LD(ref cpu.tmpbyte, cpu.af.a);
            WRITE_MEM(cpu.hl.w, cpu.tmpbyte, 4);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.af.a, cpu.bc.b*/
        void op_0x78() {
            LD(ref cpu.af.a, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a, cpu.bc.c*/
        void op_0x79() {
            LD(ref cpu.af.a, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a, cpu.de.d*/
        void op_0x7a() {
            LD(ref cpu.af.a, cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a, cpu.de.e*/
        void op_0x7b() {
            LD(ref cpu.af.a, cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a, cpu.hl.h*/
        void op_0x7c() {
            LD(ref cpu.af.a, cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a, cpu.hl.l*/
        void op_0x7d() {
            LD(ref cpu.af.a, cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a,cpu.hl.w*/
        void op_0x7e() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            LD(ref cpu.af.a, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*LD cpu.af.a, cpu.af.a*/
        void op_0x7f() {
            LD(ref cpu.af.a, cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a, cpu.bc.b*/
        void op_0x80() {
            ADD(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a, cpu.bc.c*/
        void op_0x81() {
            ADD(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a, cpu.de.d*/
        void op_0x82() {
            ADD(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a, cpu.de.e*/
        void op_0x83() {
            ADD(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a, cpu.hl.h*/
        void op_0x84() {
            ADD(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a, cpu.hl.l*/
        void op_0x85() {
            ADD(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*ADD cpu.af.a,cpu.hl.w*/
        void op_0x86() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            ADD(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*ADD cpu.af.a, cpu.af.a*/
        void op_0x87() {
            ADD(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a, cpu.bc.b*/
        void op_0x88() {
            ADC(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a, cpu.bc.c*/
        void op_0x89() {
            ADC(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a, cpu.de.d*/
        void op_0x8a() {
            ADC(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a, cpu.de.e*/
        void op_0x8b() {
            ADC(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a, cpu.hl.h*/
        void op_0x8c() {
            ADC(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a, cpu.hl.l*/
        void op_0x8d() {
            ADC(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*ADC cpu.af.a,cpu.hl.w*/
        void op_0x8e() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            ADC(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*ADC cpu.af.a, cpu.af.a*/
        void op_0x8f() {
            ADC(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.bc.b*/
        void op_0x90() {
            SUB(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.bc.c*/
        void op_0x91() {
            SUB(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.de.d*/
        void op_0x92() {
            SUB(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.de.e*/
        void op_0x93() {
            SUB(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.hl.h*/
        void op_0x94() {
            SUB(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.hl.l*/
        void op_0x95() {
            SUB(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*SUB cpu.hl.w*/
        void op_0x96() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            SUB(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*SUB cpu.af.a*/
        void op_0x97() {
            SUB(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a, cpu.bc.b*/
        void op_0x98() {
            SBC(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a, cpu.bc.c*/
        void op_0x99() {
            SBC(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a, cpu.de.d*/
        void op_0x9a() {
            SBC(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a, cpu.de.e*/
        void op_0x9b() {
            SBC(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a, cpu.hl.h*/
        void op_0x9c() {
            SBC(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a, cpu.hl.l*/
        void op_0x9d() {
            SBC(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*SBC cpu.af.a,cpu.hl.w*/
        void op_0x9e() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            SBC(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*SBC cpu.af.a, cpu.af.a*/
        void op_0x9f() {
            SBC(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.bc.b*/
        void op_0xa0() {
            AND(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.bc.c*/
        void op_0xa1() {
            AND(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.de.d*/
        void op_0xa2() {
            AND(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.de.e*/
        void op_0xa3() {
            AND(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.hl.h*/
        void op_0xa4() {
            AND(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.hl.l*/
        void op_0xa5() {
            AND(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*AND cpu.hl.w*/
        void op_0xa6() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            AND(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*AND cpu.af.a*/
        void op_0xa7() {
            AND(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.bc.b*/
        void op_0xa8() {
            XOR(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.bc.c*/
        void op_0xa9() {
            XOR(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.de.d*/
        void op_0xaa() {
            XOR(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.de.e*/
        void op_0xab() {
            XOR(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.hl.h*/
        void op_0xac() {
            XOR(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.hl.l*/
        void op_0xad() {
            XOR(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*XOR cpu.hl.w*/
        void op_0xae() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            XOR(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*XOR cpu.af.a*/
        void op_0xaf() {
            XOR(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.bc.b*/
        void op_0xb0() {
            OR(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.bc.c*/
        void op_0xb1() {
            OR(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.de.d*/
        void op_0xb2() {
            OR(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.de.e*/
        void op_0xb3() {
            OR(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.hl.h*/
        void op_0xb4() {
            OR(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.hl.l*/
        void op_0xb5() {
            OR(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*OR cpu.hl.w*/
        void op_0xb6() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            OR(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*OR cpu.af.a*/
        void op_0xb7() {
            OR(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.bc.b*/
        void op_0xb8() {
            CP(cpu.bc.b);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.bc.c*/
        void op_0xb9() {
            CP(cpu.bc.c);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.de.d*/
        void op_0xba() {
            CP(cpu.de.d);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.de.e*/
        void op_0xbb() {
            CP(cpu.de.e);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.hl.h*/
        void op_0xbc() {
            CP(cpu.hl.h);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.hl.l*/
        void op_0xbd() {
            CP(cpu.hl.l);
            T_WAIT_UNTIL(4);
        }

        /*CP cpu.hl.w*/
        void op_0xbe() {
            cpu.tmpbyte = READ_MEM(cpu.hl.w, 4);
            CP(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*CP cpu.af.a*/
        void op_0xbf() {
            CP(cpu.af.a);
            T_WAIT_UNTIL(4);
        }

        /*RET NZ*/
        void op_0xc0() {
            if((cpu.af.f & Tables.FLAG_Z) == 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*POP BC*/
        void op_0xc1() {
            POP(ref cpu.bc, /*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*JP NZ,@*/
        void op_0xc2() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_Z) == 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*JP @*/
        void op_0xc3() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            JP(cpu.tmpword.w);
            T_WAIT_UNTIL(10);
        }

        /*CALL NZ,@*/
        void op_0xc4() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_Z) == 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*PUSH BC*/
        void op_0xc5() {
            PUSH(cpu.bc, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*ADD cpu.af.a,#*/
        void op_0xc6() {
            cpu.tmpbyte = READ_OP();
            ADD(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x00*/
        void op_0xc7() {
            RST(0x00, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET Z*/
        void op_0xc8() {
            if((cpu.af.f & Tables.FLAG_Z) != 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*RET*/
        void op_0xc9() {
            RET(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*JP Z,@*/
        void op_0xca() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_Z) != 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        void op_p_CB() {
            cpu.prefix = 0xCB;
            cpu.noint_once = true;
        }

        /*CALL Z,@*/
        void op_0xcc() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_Z) != 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*CALL @*/
        void op_0xcd() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            CALL(cpu.tmpword.w, /*wr*/11, 14);
            T_WAIT_UNTIL(17);
        }

        /*ADC cpu.af.a,#*/
        void op_0xce() {
            cpu.tmpbyte = READ_OP();
            ADC(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x08*/
        void op_0xcf() {
            RST(0x08, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET NC*/
        void op_0xd0() {
            if((cpu.af.f & Tables.FLAG_C) == 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*POP cpu.de.w*/
        void op_0xd1() {
            POP(ref cpu.de, /*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*JP NC,@*/
        void op_0xd2() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_C) == 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*OUT (#),cpu.af.a*/
        void op_0xd3() {
            cpu.tmpword.w = (ushort)(READ_OP() + (cpu.af.a << 8));
            OUT_A(cpu.tmpword.w, cpu.af.a, /*wr*/8);
            T_WAIT_UNTIL(11);
        }

        /*CALL NC,@*/
        void op_0xd4() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_C) == 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*PUSH cpu.de.w*/
        void op_0xd5() {
            PUSH(cpu.de, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*SUB #*/
        void op_0xd6() {
            cpu.tmpbyte = READ_OP();
            SUB(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x10*/
        void op_0xd7() {
            RST(0x10, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET cpu.bc.c*/
        void op_0xd8() {
            if((cpu.af.f & Tables.FLAG_C) != 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*EXX*/
        void op_0xd9() {
            EXX();
            T_WAIT_UNTIL(4);
        }

        /*JP cpu.bc.c,@*/
        void op_0xda() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_C) != 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*IN cpu.af.a,(#)*/
        void op_0xdb() {
            cpu.tmpword.w = (ushort)(READ_OP() + (cpu.af.a << 8));
            IN_A(ref cpu.af.a, cpu.tmpword.w, /*rd*/8);
            T_WAIT_UNTIL(11);
        }

        /*CALL cpu.bc.c,@*/
        void op_0xdc() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_C) != 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        void op_p_DD() {
            cpu.prefix = 0xDD;
            cpu.noint_once = true;
        }

        /*SBC cpu.af.a,#*/
        void op_0xde() {
            cpu.tmpbyte = READ_OP();
            SBC(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x18*/
        void op_0xdf() {
            RST(0x18, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET PO*/
        void op_0xe0() {
            if((cpu.af.f & Tables.FLAG_P) == 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*POP cpu.hl.w*/
        void op_0xe1() {
            POP(ref cpu.hl, /*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*JP PO,@*/
        void op_0xe2() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_P) == 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*EX (cpu.sp.w),cpu.hl.w*/
        void op_0xe3() {
            cpu.tmpword.l = READ_MEM(cpu.sp.w, 4);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.sp.w + 1), 7);
            EX_MPTR(ref cpu.tmpword, ref cpu.hl);
            WRITE_MEM((cpu.sp.w), cpu.tmpword.l, 11);
            WRITE_MEM((ushort)(cpu.sp.w + 1), cpu.tmpword.h, 14);
            T_WAIT_UNTIL(19);
        }

        /*CALL PO,@*/
        void op_0xe4() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_P) == 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*PUSH cpu.hl.w*/
        void op_0xe5() {
            PUSH(cpu.hl, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*AND #*/
        void op_0xe6() {
            cpu.tmpbyte = READ_OP();
            AND(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x20*/
        void op_0xe7() {
            RST(0x20, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET PE*/
        void op_0xe8() {
            if((cpu.af.f & Tables.FLAG_P) != 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*JP cpu.hl.w*/
        void op_0xe9() {
            JP_NO_MPTR(cpu.hl.w);
            T_WAIT_UNTIL(4);
        }

        /*JP PE,@*/
        void op_0xea() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_P) != 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*EX cpu.de.w, cpu.hl.w*/
        void op_0xeb() {
            EX(ref cpu.de, ref cpu.hl);
            T_WAIT_UNTIL(4);
        }

        /*CALL PE,@*/
        void op_0xec() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_P) != 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        void op_p_ED() {
            cpu.prefix = 0xED;
            cpu.noint_once = true;
        }

        /*XOR #*/
        void op_0xee() {
            cpu.tmpbyte = READ_OP();
            XOR(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x28*/
        void op_0xef() {
            RST(0x28, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET P*/
        void op_0xf0() {
            if((cpu.af.f & Tables.FLAG_S) == 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*POP cpu.af.w*/
        void op_0xf1() {
            POP(ref cpu.af, /*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*JP P,@*/
        void op_0xf2() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_S) == 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*DI*/
        void op_0xf3() {
            DI();
            T_WAIT_UNTIL(4);
        }

        /*CALL P,@*/
        void op_0xf4() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_S) == 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*PUSH cpu.af.w*/
        void op_0xf5() {
            PUSH(cpu.af, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*OR #*/
        void op_0xf6() {
            cpu.tmpbyte = READ_OP();
            OR(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x30*/
        void op_0xf7() {
            RST(0x30, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

        /*RET M*/
        void op_0xf8() {
            if((cpu.af.f & Tables.FLAG_S) != 0) {
                RET(/*rd*/5, 8);
                T_WAIT_UNTIL(11);
            } else { T_WAIT_UNTIL(5); }
        }

        /*LD cpu.sp.w, cpu.hl.w*/
        void op_0xf9() {
            LD16(ref cpu.sp, cpu.hl);
            T_WAIT_UNTIL(6);
        }

        /*JP M,@*/
        void op_0xfa() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_S) != 0) {
                JP(cpu.tmpword.w);
                T_WAIT_UNTIL(10);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        /*EI*/
        void op_0xfb() {
            EI();
            T_WAIT_UNTIL(4);
        }

        /*CALL M,@*/
        void op_0xfc() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            if((cpu.af.f & Tables.FLAG_S) != 0) {
                CALL(cpu.tmpword.w, /*wr*/11, 14);
                T_WAIT_UNTIL(17);
            } else { T_WAIT_UNTIL(10); cpu.memptr.w = cpu.tmpword.w; }
        }

        void op_p_FD() {
            cpu.prefix = 0xFD;
            cpu.noint_once = true;
        }

        /*CP #*/
        void op_0xfe() {
            cpu.tmpbyte = READ_OP();
            CP(cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }

        /*RST 0x38*/
        void op_0xff() {
            RST(0x38, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }

    }
}
