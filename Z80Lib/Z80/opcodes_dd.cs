﻿
namespace Z80Lib {
    partial class Z80 {
        OperationDelegate[] opcodes_dd;

        void init_opcodes_dd() {
            opcodes_dd = new OperationDelegate[] {
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_DD_0x09, null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_DD_0x19, null      , null      ,
                null      , null      , null      , null      ,
                null      , op_DD_0x21, op_DD_0x22, op_DD_0x23,
                op_DD_0x24, op_DD_0x25, op_DD_0x26, null      ,
                null      , op_DD_0x29, op_DD_0x2a, op_DD_0x2b,
                op_DD_0x2c, op_DD_0x2d, op_DD_0x2e, null      ,
                null      , null      , null      , null      ,
                op_DD_0x34, op_DD_0x35, op_DD_0x36, null      ,
                null      , op_DD_0x39, null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                op_DD_0x44, op_DD_0x45, op_DD_0x46, null      ,
                null      , null      , null      , null      ,
                op_DD_0x4c, op_DD_0x4d, op_DD_0x4e, null      ,
                null      , null      , null      , null      ,
                op_DD_0x54, op_DD_0x55, op_DD_0x56, null      ,
                null      , null      , null      , null      ,
                op_DD_0x5c, op_DD_0x5d, op_DD_0x5e, null      ,
                op_DD_0x60, op_DD_0x61, op_DD_0x62, op_DD_0x63,
                op_DD_0x64, op_DD_0x65, op_DD_0x66, op_DD_0x67,
                op_DD_0x68, op_DD_0x69, op_DD_0x6a, op_DD_0x6b,
                op_DD_0x6c, op_DD_0x6d, op_DD_0x6e, op_DD_0x6f,
                op_DD_0x70, op_DD_0x71, op_DD_0x72, op_DD_0x73,
                op_DD_0x74, op_DD_0x75, null      , op_DD_0x77,
                null      , null      , null      , null      ,
                op_DD_0x7c, op_DD_0x7d, op_DD_0x7e, null      ,
                null      , null      , null      , null      ,
                op_DD_0x84, op_DD_0x85, op_DD_0x86, null      ,
                null      , null      , null      , null      ,
                op_DD_0x8c, op_DD_0x8d, op_DD_0x8e, null      ,
                null      , null      , null      , null      ,
                op_DD_0x94, op_DD_0x95, op_DD_0x96, null      ,
                null      , null      , null      , null      ,
                op_DD_0x9c, op_DD_0x9d, op_DD_0x9e, null      ,
                null      , null      , null      , null      ,
                op_DD_0xa4, op_DD_0xa5, op_DD_0xa6, null      ,
                null      , null      , null      , null      ,
                op_DD_0xac, op_DD_0xad, op_DD_0xae, null      ,
                null      , null      , null      , null      ,
                op_DD_0xb4, op_DD_0xb5, op_DD_0xb6, null      ,
                null      , null      , null      , null      ,
                op_DD_0xbc, op_DD_0xbd, op_DD_0xbe, null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_DD_0xe1, null      , op_DD_0xe3,
                null      , op_DD_0xe5, null      , null      ,
                null      , op_DD_0xe9, null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , op_DD_0xf9, null      , null      ,
                null      , null      , null      , null      
            };
        }

        /*ADD cpu.ix,cpu.bc*/
        void op_DD_0x09() {
            ADD16(ref cpu.ix, cpu.bc);
            T_WAIT_UNTIL(11);
        }
        /*ADD cpu.ix,cpu.de*/
        void op_DD_0x19() {
            ADD16(ref cpu.ix, cpu.de);
            T_WAIT_UNTIL(11);
        }
        /*LD cpu.ix,@*/
        void op_DD_0x21() {
            cpu.tmpword.l = READ_OP();
            cpu.tmpword.h = READ_OP();
            LD16(ref cpu.ix, cpu.tmpword);
            T_WAIT_UNTIL(10);
        }
        /*LD (@),cpu.ix*/
        void op_DD_0x22() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.ix, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }
        /*INC cpu.ix*/
        void op_DD_0x23() {
            INC16(ref cpu.ix);
            T_WAIT_UNTIL(6);
        }
        /*INC cpu.ix.h*/
        void op_DD_0x24() {
            INC(ref cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*DEC cpu.ix.h*/
        void op_DD_0x25() {
            DEC(ref cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.h,#*/
        void op_DD_0x26() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.ix.h, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }
        /*ADD cpu.ix,cpu.ix*/
        void op_DD_0x29() {
            ADD16(ref cpu.ix, cpu.ix);
            T_WAIT_UNTIL(11);
        }
        /*LD cpu.ix,(@)*/
        void op_DD_0x2a() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.ix, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }
        /*DEC cpu.ix*/
        void op_DD_0x2b() {
            DEC16(ref cpu.ix);
            T_WAIT_UNTIL(6);
        }
        /*INC cpu.ix.l*/
        void op_DD_0x2c() {
            INC(ref cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*DEC cpu.ix.l*/
        void op_DD_0x2d() {
            DEC(ref cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,#*/
        void op_DD_0x2e() {
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.ix.l, cpu.tmpbyte);
            T_WAIT_UNTIL(7);
        }
        /*INC (cpu.ix.w+$)*/
        void op_DD_0x34() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            INC(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }
        /*DEC (cpu.ix.w+$)*/
        void op_DD_0x35() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            DEC(ref cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 16);
            T_WAIT_UNTIL(19);
        }
        /*LD (cpu.ix.w+$),#*/
        void op_DD_0x36() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_OP();
            LD(ref cpu.tmpbyte, cpu.tmpbyte);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*ADD cpu.ix,cpu.sp*/
        void op_DD_0x39() {
            ADD16(ref cpu.ix, cpu.sp);
            T_WAIT_UNTIL(11);
        }
        /*LD cpu.bc.b,cpu.ix.h*/
        void op_DD_0x44() {
            LD(ref cpu.bc.b, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.bc.b,cpu.ix.l*/
        void op_DD_0x45() {
            LD(ref cpu.bc.b, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.bc.b,(cpu.ix.w+$)*/
        void op_DD_0x46() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.bc.b, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.bc.c,cpu.ix.h*/
        void op_DD_0x4c() {
            LD(ref cpu.bc.c, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.bc.c,cpu.ix.l*/
        void op_DD_0x4d() {
            LD(ref cpu.bc.c, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.bc.c,(cpu.ix.w+$)*/
        void op_DD_0x4e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.bc.c, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.de.d,cpu.ix.h*/
        void op_DD_0x54() {
            LD(ref cpu.de.d, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.de.d,cpu.ix.l*/
        void op_DD_0x55() {
            LD(ref cpu.de.d, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.de.d,(cpu.ix.w+$)*/
        void op_DD_0x56() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.de.d, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.de.e,cpu.ix.h*/
        void op_DD_0x5c() {
            LD(ref cpu.de.e, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.de.e,cpu.ix.l*/
        void op_DD_0x5d() {
            LD(ref cpu.de.e, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.de.e,(cpu.ix.w+$)*/
        void op_DD_0x5e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.de.e, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.ix.h,cpu.bc.b*/
        void op_DD_0x60() {
            LD(ref cpu.ix.h, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.h,cpu.bc.c*/
        void op_DD_0x61() {
            LD(ref cpu.ix.h, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.h,cpu.de.d*/
        void op_DD_0x62() {
            LD(ref cpu.ix.h, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.h,cpu.de.e*/
        void op_DD_0x63() {
            LD(ref cpu.ix.h, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.h,cpu.ix.h*/
        void op_DD_0x64() {
            LD(ref cpu.ix.h, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.h,cpu.ix.l*/
        void op_DD_0x65() {
            LD(ref cpu.ix.h, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD H,(cpu.ix.w+$)*/
        void op_DD_0x66() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.hl.h, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.ix.h,cpu.af.a*/
        void op_DD_0x67() {
            LD(ref cpu.ix.h, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,cpu.bc.b*/
        void op_DD_0x68() {
            LD(ref cpu.ix.l, cpu.bc.b);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,cpu.bc.c*/
        void op_DD_0x69() {
            LD(ref cpu.ix.l, cpu.bc.c);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,cpu.de.d*/
        void op_DD_0x6a() {
            LD(ref cpu.ix.l, cpu.de.d);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,cpu.de.e*/
        void op_DD_0x6b() {
            LD(ref cpu.ix.l, cpu.de.e);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,cpu.ix.h*/
        void op_DD_0x6c() {
            LD(ref cpu.ix.l, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.ix.l,cpu.ix.l*/
        void op_DD_0x6d() {
            LD(ref cpu.ix.l, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD L,(cpu.ix.w+$)*/
        void op_DD_0x6e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.hl.l, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.ix.l,cpu.af.a*/
        void op_DD_0x6f() {
            LD(ref cpu.ix.l, cpu.af.a);
            T_WAIT_UNTIL(4);
        }
        /*LD (cpu.ix.w+$),cpu.bc.b*/
        void op_DD_0x70() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.bc.b);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD (cpu.ix.w+$),cpu.bc.c*/
        void op_DD_0x71() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.bc.c);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD (cpu.ix.w+$),cpu.de.d*/
        void op_DD_0x72() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.de.d);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD (cpu.ix.w+$),cpu.de.e*/
        void op_DD_0x73() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.de.e);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD (cpu.ix.w+$),H*/
        void op_DD_0x74() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.hl.h);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD (cpu.ix.w+$),L*/
        void op_DD_0x75() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.hl.l);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD (cpu.ix.w+$),cpu.af.a*/
        void op_DD_0x77() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            LD(ref cpu.tmpbyte, cpu.af.a);
            WRITE_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), cpu.tmpbyte, 12);
            T_WAIT_UNTIL(15);
        }
        /*LD cpu.af.a,cpu.ix.h*/
        void op_DD_0x7c() {
            LD(ref cpu.af.a, cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.af.a,cpu.ix.l*/
        void op_DD_0x7d() {
            LD(ref cpu.af.a, cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.af.a,(cpu.ix.w+$)*/
        void op_DD_0x7e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            LD(ref cpu.af.a, cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*ADD cpu.af.a,cpu.ix.h*/
        void op_DD_0x84() {
            ADD(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*ADD cpu.af.a,cpu.ix.l*/
        void op_DD_0x85() {
            ADD(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*ADD cpu.af.a,(cpu.ix.w+$)*/
        void op_DD_0x86() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            ADD(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*ADC cpu.af.a,cpu.ix.h*/
        void op_DD_0x8c() {
            ADC(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*ADC cpu.af.a,cpu.ix.l*/
        void op_DD_0x8d() {
            ADC(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*ADC cpu.af.a,(cpu.ix.w+$)*/
        void op_DD_0x8e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            ADC(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*SUB cpu.ix.h*/
        void op_DD_0x94() {
            SUB(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*SUB cpu.ix.l*/
        void op_DD_0x95() {
            SUB(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*SUB (cpu.ix.w+$)*/
        void op_DD_0x96() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SUB(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*SBC cpu.af.a,cpu.ix.h*/
        void op_DD_0x9c() {
            SBC(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*SBC cpu.af.a,cpu.ix.l*/
        void op_DD_0x9d() {
            SBC(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*SBC cpu.af.a,(cpu.ix.w+$)*/
        void op_DD_0x9e() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            SBC(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*AND cpu.ix.h*/
        void op_DD_0xa4() {
            AND(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*AND cpu.ix.l*/
        void op_DD_0xa5() {
            AND(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*AND (cpu.ix.w+$)*/
        void op_DD_0xa6() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            AND(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*XOR cpu.ix.h*/
        void op_DD_0xac() {
            XOR(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*XOR cpu.ix.l*/
        void op_DD_0xad() {
            XOR(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*XOR (cpu.ix.w+$)*/
        void op_DD_0xae() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            XOR(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*OR cpu.ix.h*/
        void op_DD_0xb4() {
            OR(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*OR cpu.ix.l*/
        void op_DD_0xb5() {
            OR(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*OR (cpu.ix.w+$)*/
        void op_DD_0xb6() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            OR(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }
        /*CP cpu.ix.h*/
        void op_DD_0xbc() {
            CP(cpu.ix.h);
            T_WAIT_UNTIL(4);
        }
        /*CP cpu.ix.l*/
        void op_DD_0xbd() {
            CP(cpu.ix.l);
            T_WAIT_UNTIL(4);
        }
        /*CP (cpu.ix.w+$)*/
        void op_DD_0xbe() {
            cpu.tmpbyte = READ_OP();
            cpu.tmpbyte_s = (sbyte)((cpu.tmpbyte & 0x80) != 0 ? -(((~cpu.tmpbyte) & 0x7f) + 1) : cpu.tmpbyte);
            cpu.memptr.w = (ushort)(cpu.ix.w + cpu.tmpbyte_s);
            cpu.tmpbyte = READ_MEM((ushort)(cpu.ix.w + cpu.tmpbyte_s), 12);
            CP(cpu.tmpbyte);
            T_WAIT_UNTIL(15);
        }

        /*POP cpu.ix*/
        void op_DD_0xe1() {
            POP(ref cpu.ix, /*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }
        /*EX (cpu.sp),cpu.ix*/
        void op_DD_0xe3() {
            cpu.tmpword.l = READ_MEM(cpu.sp.w, 4);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.sp.w + 1), 7);
            EX_MPTR(ref cpu.tmpword, ref cpu.ix);
            WRITE_MEM(cpu.sp.w, cpu.tmpword.l, 11);
            WRITE_MEM((ushort)(cpu.sp.w + 1), cpu.tmpword.h, 14);
            T_WAIT_UNTIL(19);
        }
        /*PUSH cpu.ix*/
        void op_DD_0xe5() {
            PUSH(cpu.ix, /*wr*/5, 8);
            T_WAIT_UNTIL(11);
        }
        /*JP cpu.ix*/
        void op_DD_0xe9() {
            JP_NO_MPTR(cpu.ix.w);
            T_WAIT_UNTIL(4);
        }
        /*LD cpu.sp,cpu.ix*/
        void op_DD_0xf9() {
            LD16(ref cpu.sp, cpu.ix);
            T_WAIT_UNTIL(6);
        }

    }
}
