﻿namespace Z80Lib {
    partial class Z80 {
        OperationDelegate[] opcodes_ed;

        void init_opcodes_ed() {
            opcodes_ed = new OperationDelegate[] { 
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                op_ED_0x40, op_ED_0x41, op_ED_0x42, op_ED_0x43,
                op_ED_0x44, op_ED_0x45, op_ED_0x46, op_ED_0x47,
                op_ED_0x48, op_ED_0x49, op_ED_0x4a, op_ED_0x4b,
                op_ED_0x4c, op_ED_0x4d, op_ED_0x4e, op_ED_0x4f,
                op_ED_0x50, op_ED_0x51, op_ED_0x52, op_ED_0x53,
                op_ED_0x54, op_ED_0x55, op_ED_0x56, op_ED_0x57,
                op_ED_0x58, op_ED_0x59, op_ED_0x5a, op_ED_0x5b,
                op_ED_0x5c, op_ED_0x5d, op_ED_0x5e, op_ED_0x5f,
                op_ED_0x60, op_ED_0x61, op_ED_0x62, op_ED_0x63,
                op_ED_0x64, op_ED_0x65, op_ED_0x66, op_ED_0x67,
                op_ED_0x68, op_ED_0x69, op_ED_0x6a, op_ED_0x6b,
                op_ED_0x6c, op_ED_0x6d, op_ED_0x6e, op_ED_0x6f,
                op_ED_0x70, op_ED_0x71, op_ED_0x72, op_ED_0x73,
                op_ED_0x74, op_ED_0x75, op_ED_0x76, null      ,
                op_ED_0x78, op_ED_0x79, op_ED_0x7a, op_ED_0x7b,
                op_ED_0x7c, op_ED_0x7d, op_ED_0x7e, null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                op_ED_0xa0, op_ED_0xa1, op_ED_0xa2, op_ED_0xa3,
                null      , null      , null      , null      ,
                op_ED_0xa8, op_ED_0xa9, op_ED_0xaa, op_ED_0xab,
                null      , null      , null      , null      ,
                op_ED_0xb0, op_ED_0xb1, op_ED_0xb2, op_ED_0xb3,
                null      , null      , null      , null      ,
                op_ED_0xb8, op_ED_0xb9, op_ED_0xba, op_ED_0xbb,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      ,
                null      , null      , null      , null      
            };
        }

        /*IN cpu.bc.b,(cpu.bc.c)*/
        void op_ED_0x40() {
            IN(ref cpu.bc.b, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.bc.b*/
        void op_ED_0x41() {
            OUT(cpu.bc.w, cpu.bc.b, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*SBC cpu.hl.w,cpu.bc.w*/
        void op_ED_0x42() {
            SBC16(cpu.hl.w, cpu.bc.w);
            T_WAIT_UNTIL(11);
        }

        /*LD (@),cpu.bc.w*/
        void op_ED_0x43() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.bc, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x44() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETN*/
        void op_ED_0x45() {
            RETN(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 0*/
        void op_ED_0x46() {
            IM(IM_MODE.IM0);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.i,cpu.af.a*/
        void op_ED_0x47() {
            LD(ref cpu.i, cpu.af.a);
            T_WAIT_UNTIL(5);
        }

        /*IN cpu.bc.c,(cpu.bc.c)*/
        void op_ED_0x48() {
            IN(ref cpu.bc.c, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.bc.c*/
        void op_ED_0x49() {
            OUT(cpu.bc.w, cpu.bc.c, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*ADC cpu.hl.w,cpu.bc.w*/
        void op_ED_0x4a() {
            ADC16(cpu.hl.w, cpu.bc.w);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.bc.w,(@)*/
        void op_ED_0x4b() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.bc, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x4c() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETI*/
        void op_ED_0x4d() {
            RETI(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 0*/
        void op_ED_0x4e() {
            IM(IM_MODE.IM0);
            T_WAIT_UNTIL(4);
        }

        /*LD R,cpu.af.a*/
        void op_ED_0x4f() {
            LD_R_A();
            T_WAIT_UNTIL(5);
        }

        /*IN cpu.de.d,(cpu.bc.c)*/
        void op_ED_0x50() {
            IN(ref cpu.de.d, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.de.d*/
        void op_ED_0x51() {
            OUT(cpu.bc.w, cpu.de.d, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*SBC cpu.hl.w,cpu.de.w*/
        void op_ED_0x52() {
            SBC16(cpu.hl.w, cpu.de.w);
            T_WAIT_UNTIL(11);
        }

        /*LD (@),cpu.de.w*/
        void op_ED_0x53() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.de, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x54() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETN*/
        void op_ED_0x55() {
            RETN(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 1*/
        void op_ED_0x56() {
            IM(IM_MODE.IM1);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a,cpu.i*/
        void op_ED_0x57() {
            LD_A_I();
            T_WAIT_UNTIL(5);
        }

        /*IN cpu.de.e,(cpu.bc.c)*/
        void op_ED_0x58() {
            IN(ref cpu.de.e, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.de.e*/
        void op_ED_0x59() {
            OUT(cpu.bc.w, cpu.de.e, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*ADC cpu.hl.w,cpu.de.w*/
        void op_ED_0x5a() {
            ADC16(cpu.hl.w, cpu.de.w);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.de.w,(@)*/
        void op_ED_0x5b() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.de, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x5c() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETI*/
        void op_ED_0x5d() {
            RETI(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 2*/
        void op_ED_0x5e() {
            IM(IM_MODE.IM2);
            T_WAIT_UNTIL(4);
        }

        /*LD cpu.af.a,R*/
        void op_ED_0x5f() {
            LD_A_R();
            T_WAIT_UNTIL(5);
        }

        /*IN cpu.hl.h,(cpu.bc.c)*/
        void op_ED_0x60() {
            IN(ref cpu.hl.h, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.hl.h*/
        void op_ED_0x61() {
            OUT(cpu.bc.w, cpu.hl.h, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*SBC cpu.hl.w,cpu.hl.w*/
        void op_ED_0x62() {
            SBC16(cpu.hl.w, cpu.hl.w);
            T_WAIT_UNTIL(11);
        }

        /*LD (@),cpu.hl.w*/
        void op_ED_0x63() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.hl, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x64() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETN*/
        void op_ED_0x65() {
            RETN(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 0*/
        void op_ED_0x66() {
            IM(IM_MODE.IM0);
            T_WAIT_UNTIL(4);
        }

        /*RRD*/
        void op_ED_0x67() {
            RRD(/*rd*/4, /*wr*/11);
            T_WAIT_UNTIL(14);
        }

        /*IN cpu.hl.l,(cpu.bc.c)*/
        void op_ED_0x68() {
            IN(ref cpu.hl.l, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.hl.l*/
        void op_ED_0x69() {
            OUT(cpu.bc.w, cpu.hl.l, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*ADC cpu.hl.w,cpu.hl.w*/
        void op_ED_0x6a() {
            ADC16(cpu.hl.w, cpu.hl.w);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.hl.w,(@)*/
        void op_ED_0x6b() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.hl, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x6c() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETI*/
        void op_ED_0x6d() {
            RETI(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 0*/
        void op_ED_0x6e() {
            IM(IM_MODE.IM0);
            T_WAIT_UNTIL(4);
        }

        /*RLD*/
        void op_ED_0x6f() {
            RLD(/*rd*/4, /*wr*/11);
            T_WAIT_UNTIL(14);
        }

        /*IN_F (cpu.bc.c)*/
        void op_ED_0x70() {
            IN_F(cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),0*/
        void op_ED_0x71() {
            OUT(cpu.bc.w, 0, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*SBC cpu.hl.w,cpu.sp.w*/
        void op_ED_0x72() {
            SBC16(cpu.hl.w, cpu.sp.w);
            T_WAIT_UNTIL(11);
        }

        /*LD (@),cpu.sp.w*/
        void op_ED_0x73() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            LD_RP_TO_ADDR_MPTR_16(ref cpu.tmpword, cpu.sp, cpu.tmpaddr.w);
            WRITE_MEM(cpu.tmpaddr.w, cpu.tmpword.l, 10);
            WRITE_MEM((ushort)(cpu.tmpaddr.w + 1), cpu.tmpword.h, 13);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x74() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETN*/
        void op_ED_0x75() {
            RETN(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 1*/
        void op_ED_0x76() {
            IM(IM_MODE.IM1);
            T_WAIT_UNTIL(4);
        }

        /*IN cpu.af.a,(cpu.bc.c)*/
        void op_ED_0x78() {
            IN(ref cpu.af.a, cpu.bc.w, /*rd*/5);
            T_WAIT_UNTIL(8);
        }

        /*OUT (cpu.bc.c),cpu.af.a*/
        void op_ED_0x79() {
            OUT(cpu.bc.w, cpu.af.a, /*wr*/5);
            T_WAIT_UNTIL(8);
        }

        /*ADC cpu.hl.w,cpu.sp.w*/
        void op_ED_0x7a() {
            ADC16(cpu.hl.w, cpu.sp.w);
            T_WAIT_UNTIL(11);
        }

        /*LD cpu.sp.w,(@)*/
        void op_ED_0x7b() {
            cpu.tmpaddr.l = READ_OP();
            cpu.tmpaddr.h = READ_OP();
            cpu.tmpword.l = READ_MEM(cpu.tmpaddr.w, 10);
            cpu.tmpword.h = READ_MEM((ushort)(cpu.tmpaddr.w + 1), 13);
            LD_RP_FROM_ADDR_MPTR_16(ref cpu.sp, cpu.tmpword, cpu.tmpaddr.w);
            T_WAIT_UNTIL(16);
        }

        /*NEG*/
        void op_ED_0x7c() {
            NEG();
            T_WAIT_UNTIL(4);
        }

        /*RETI*/
        void op_ED_0x7d() {
            RETI(/*rd*/4, 7);
            T_WAIT_UNTIL(10);
        }

        /*IM 2*/
        void op_ED_0x7e() {
            IM(IM_MODE.IM2);
            T_WAIT_UNTIL(4);
        }

        /*LDI*/
        void op_ED_0xa0() {
            LDI(/*rd*/4, /*wr*/7);
            T_WAIT_UNTIL(12);
        }

        /*CPI*/
        void op_ED_0xa1() {
            CPI(/*rd*/4);
            T_WAIT_UNTIL(12);
        }

        /*INI*/
        void op_ED_0xa2() {
            INI(/*rd*/6, /*wr*/9);
            T_WAIT_UNTIL(12);
        }

        /*OUTI*/
        void op_ED_0xa3() {
            OUTI(/*rd*/5, /*wr*/9);
            T_WAIT_UNTIL(12);
        }

        /*LDD*/
        void op_ED_0xa8() {
            LDD(/*rd*/4, /*wr*/7);
            T_WAIT_UNTIL(12);
        }

        /*CPD*/
        void op_ED_0xa9() {
            CPD(/*rd*/4);
            T_WAIT_UNTIL(12);
        }

        /*IND*/
        void op_ED_0xaa() {
            IND(/*rd*/6, /*wr*/9);
            T_WAIT_UNTIL(12);
        }

        /*OUTD*/
        void op_ED_0xab() {
            OUTD(/*rd*/5, /*wr*/9);
            T_WAIT_UNTIL(12);
        }

        /*LDIR*/
        void op_ED_0xb0() {
            LDIR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/4, /*wr*/7);
        }

        /*CPIR*/
        void op_ED_0xb1() {
            CPIR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/4);
        }

        /*INIR*/
        void op_ED_0xb2() {
            INIR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/6, /*wr*/9);
        }

        /*OTIR*/
        void op_ED_0xb3() {
            OTIR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/5, /*wr*/9);
        }

        /*LDDR*/
        void op_ED_0xb8() {
            LDDR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/4, /*wr*/7);
        }

        /*CPDR*/
        void op_ED_0xb9() {
            CPDR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/4);
        }

        /*INDR*/
        void op_ED_0xba() {
            INDR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/6, /*wr*/9);
        }

        /*OTDR*/
        void op_ED_0xbb() {
            OTDR(/*t:*/ /*t1*/12,/*t2*/17, /*rd*/5, /*wr*/9);
        }

    }
}
