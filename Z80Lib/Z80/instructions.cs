﻿
// ReSharper disable RedundantAssignment
namespace Z80Lib {
    partial class Z80 {

        void AND(byte value) {
            cpu.af.a &= value;
            cpu.af.f = (byte)(Tables.FLAG_H | Tables.sz53p_table[cpu.af.a]);
        }

        void ADC(byte value) {
            ushort adctemp = (ushort)(cpu.af.a + value + (cpu.af.f & Tables.FLAG_C));
            byte lookup = (byte)(
                  ((cpu.af.a & 0x88) >> 3)
                | ((value & 0x88) >> 2)
                | ((adctemp & 0x88) >> 1)
                );
            cpu.af.a = (byte)adctemp;
            cpu.af.f = (byte)(
                  ((adctemp & 0x100) != 0 ? Tables.FLAG_C : (byte)0)
                | Tables.halfcarry_add_table[lookup & 0x07]
                | Tables.overflow_add_table[lookup >> 4]
                | Tables.sz53_table[cpu.af.a]
                );
        }

        void ADC16(ushort hl, ushort value) {
            int add16temp = cpu.hl.w + value + (cpu.af.f & Tables.FLAG_C);

            byte lookup = (byte)(
                          ((cpu.hl.w & 0x8800) >> 11)
                        | ((value & 0x8800) >> 10)
                        | ((add16temp & 0x8800) >> 9)
                        );

            cpu.memptr.w = (ushort)(hl + 1);

            cpu.hl.w = (ushort)add16temp;

            cpu.af.f = (byte)(
                  ((add16temp & 0x10000) != 0 ? Tables.FLAG_C : (byte)0)
                | Tables.overflow_add_table[lookup >> 4]
                | (cpu.hl.h & (Tables.FLAG_3 | Tables.FLAG_5 | Tables.FLAG_S))
                | Tables.halfcarry_add_table[lookup & 0x07]
                | (cpu.hl.w != 0 ? (byte)0 : Tables.FLAG_Z)
                );
        }

        void ADD(byte value) {
            ushort addtemp = (ushort)(cpu.af.a + value);
            byte lookup = (byte)(
                          ((cpu.af.a & 0x88) >> 3)
                        | ((value & 0x88) >> 2)
                        | ((addtemp & 0x88) >> 1)
                        );
            cpu.af.a = (byte)addtemp;
            cpu.af.f = (byte)(
                          ((addtemp & 0x100) != 0 ? Tables.FLAG_C : (byte)0)
                        | Tables.halfcarry_add_table[lookup & 0x07]
                        | Tables.overflow_add_table[lookup >> 4]
                        | Tables.sz53_table[cpu.af.a]
                    );
        }

        void ADD16(ref RegPair value1, RegPair value2) {
            int add16temp = value1.w + value2.w;

            byte lookup = (byte)(
                              ((value1.w & 0x0800) >> 11)
                            | ((value2.w & 0x0800) >> 10)
                            | ((add16temp & 0x0800) >> 9)
                        );

            cpu.memptr.w = (ushort)(value1.w + 1);
            value1.w = (ushort)add16temp;

            cpu.af.f = (byte)(
                        (cpu.af.f & (Tables.FLAG_V | Tables.FLAG_Z | Tables.FLAG_S))
                        | ((add16temp & 0x10000) != 0 ? Tables.FLAG_C : (byte)0)
                        | ((add16temp >> 8) & (Tables.FLAG_3 | Tables.FLAG_5))
                        | Tables.halfcarry_add_table[lookup]
                    );
        }

        void BIT(byte bit, byte value) {
            cpu.af.f = (byte)(
                      (cpu.af.f & Tables.FLAG_C)
                    | Tables.FLAG_H
                    | Tables.sz53p_table[value & (0x01 << bit)]
                    | (value & 0x28)
                );
        }

        /**
         * BIT n,(IX+d/IY+d) and BIT n,(HL)
         */
        void BIT_MPTR(byte bit, byte value) {
            cpu.af.f = (byte)(
                      (cpu.af.f & Tables.FLAG_C)
                    | Tables.FLAG_H
                    | (Tables.sz53p_table[value & (0x01 << bit)] & 0xD7)
                    | (cpu.memptr.h & 0x28)
                );
        }

        void CALL(ushort addr, int t_state1, int t_state2) {
            PUSH(cpu.pc, t_state1, t_state2);
            cpu.pc.w = cpu.memptr.w = addr;
        }

        void CP(byte value) {
            int cptemp = cpu.af.a - value;
            byte lookup = (byte)(
                              ((cpu.af.a & 0x88) >> 3)
                            | ((value & 0x88) >> 2)
                            | ((cptemp & 0x88) >> 1)
                        );
            cpu.af.f = (byte)(
                        ((cptemp & 0x100) != 0 ? Tables.FLAG_C : (cptemp != 0 ? (byte)0 : Tables.FLAG_Z))
                        | Tables.FLAG_N
                        | Tables.halfcarry_sub_table[lookup & 0x07]
                        | Tables.overflow_sub_table[lookup >> 4]
                        | (value & (Tables.FLAG_3 | Tables.FLAG_5))
                        | (cptemp & Tables.FLAG_S)
                    );
        }

        void DEC(ref byte value) {
            cpu.af.f = (byte)(
                    (cpu.af.f & Tables.FLAG_C) | ((value & 0x0f) != 0 ? (byte)0 : Tables.FLAG_H) | Tables.FLAG_N
                );
            value--;
            cpu.af.f |= (byte)(
                          (value == 0x7f ? Tables.FLAG_V : (byte)0)
                        | Tables.sz53_table[value]
                    );
        }

        void DEC16(ref RegPair value) {
            value.w--;
        }

        void INC(ref byte value) {
            value++;
            cpu.af.f = (byte)(
                          (cpu.af.f & Tables.FLAG_C) | (value == 0x80 ? Tables.FLAG_V : (byte)0)
                        | ((value & 0x0f) != 0 ? (byte)0 : Tables.FLAG_H)
                        | Tables.sz53_table[(value)]
                    );
        }

        void INC16(ref RegPair value) {
            value.w++;
        }

        static void LD(ref byte dst, byte src) {
            dst = src;
        }

        /**
         * ld (nnnn|BC|DE), A
         */
        void LD_A_TO_ADDR_MPTR(ref byte dest, byte src, ushort addr) {
            dest = src;
            cpu.memptr.h = cpu.af.a;
            //cpu.memptr.l = (byte)((addr + 1) & 0xFF);
            cpu.memptr.l = (byte)(addr + 1);
        }

        /**
         * ld a,(BC|DE|nnnn)
         */
        void LD_A_FROM_ADDR_MPTR(ref byte dest, byte src, ushort addr) {
            dest = src;
            cpu.memptr.w = (ushort)(addr + 1);
        }

        static void LD16(ref RegPair dest, RegPair src) {
            dest.w = src.w;
        }

        /**
         * xxCB codes call this with bytes for some reason
         */

        static void LD16(ref byte dest, byte src) {
            dest = src;
        }

        /**
         * ld (nnnn),BC|DE|SP|HL|IX|IY
         */
        void LD_RP_TO_ADDR_MPTR_16(ref RegPair dest, RegPair src, ushort addr) {
            dest.w = src.w;
            cpu.memptr.w = (ushort)(addr + 1);
        }

        /**
         * ld BC|DE|SP|HL|IX|IY,(nnnn)
         */
        void LD_RP_FROM_ADDR_MPTR_16(ref RegPair dest, RegPair src, ushort addr) {
            dest.w = src.w;
            cpu.memptr.w = (ushort)(addr + 1);
        }

        void JP_NO_MPTR(ushort addr) {
            cpu.pc.w = addr;
        }

        void JP(ushort addr) {
            cpu.pc.w = cpu.memptr.w = addr;
        }

        void JR(sbyte offset) {
            cpu.pc.w = (ushort)(cpu.pc.w + offset);
            cpu.memptr.w = cpu.pc.w;
        }

        void OR(byte value) {
            cpu.af.a = (byte)(cpu.af.a | value);
            cpu.af.f = Tables.sz53p_table[cpu.af.a];
        }

        void OUT(ushort port, byte reg, int wr) {
            WRITE_PORT(port, reg, wr);
            cpu.memptr.w = (ushort)(port + 1);
        }

        /**
         * OUT (nn),A
         */
        void OUT_A(ushort port, byte reg, int wr) {
            WRITE_PORT(port, reg, wr);
            cpu.memptr.l = (byte)(port + 1);
            cpu.memptr.h = cpu.af.a;
        }

        void IN(ref byte reg, ushort port, int rd) {
            reg = READ_PORT(port, rd);
            cpu.af.f = (byte)((cpu.af.f & Tables.FLAG_C) | Tables.sz53p_table[reg]);
            cpu.memptr.w = (ushort)(port + 1);
        }

        /**
         * IN A,(nn)
         */
        void IN_A(ref byte reg, ushort port, int rd) {
            reg = READ_PORT(port, rd);
            cpu.memptr.w = (ushort)(port + 1);
        }

        void IN_F(ushort port, int rd) {
            byte val = 0;
            IN(ref val, port, rd);
        }

        void POP(ref RegPair rp, int t_state1, int t_state2) {
            rp.l = READ_MEM(cpu.sp.w++, t_state1);
            rp.h = READ_MEM(cpu.sp.w++, t_state2);
        }

        void PUSH(RegPair rp, int t_state1, int t_state2) {
            WRITE_MEM(--cpu.sp.w, rp.h, t_state1);
            WRITE_MEM(--cpu.sp.w, rp.l, t_state2);
        }

        void RET(int t_state1, int t_state2) {
            POP(ref cpu.pc, t_state1, t_state2);
            cpu.memptr.w = cpu.pc.w;
        }

        void RL(ref byte value) {
            byte rltemp = value;
            value = (byte)((value << 1) | (cpu.af.f & Tables.FLAG_C));
            cpu.af.f = (byte)((rltemp >> 7) | Tables.sz53p_table[value]);
        }

        void RLC(ref byte value) {
            value = (byte)((value << 1) | (value >> 7));
            cpu.af.f = (byte)((value & Tables.FLAG_C) | Tables.sz53p_table[value]);
        }

        void RR(ref byte value) {
            byte rrtemp = value;
            value = (byte)((value >> 1) | (cpu.af.f << 7));
            cpu.af.f = (byte)((rrtemp & Tables.FLAG_C) | Tables.sz53p_table[value]);
        }

        void RRC(ref byte value) {
            cpu.af.f = (byte)(value & Tables.FLAG_C);
            value = (byte)((value >> 1) | (value << 7));
            cpu.af.f |= Tables.sz53p_table[value];
        }

        void RST(ushort value, int w1, int w2) {
            PUSH(cpu.pc, w1, w2);
            cpu.pc.w = value;
            cpu.memptr.w = cpu.pc.w;
        }

        void SBC(byte value) {
            int sbctemp = cpu.af.a - value - (cpu.af.f & Tables.FLAG_C);
            byte lookup = (byte)(
                            ((cpu.af.a & 0x88) >> 3)
                            | ((value & 0x88) >> 2)
                            | ((sbctemp & 0x88) >> 1)
                        );
            cpu.af.a = (byte)sbctemp;
            cpu.af.f = (byte)(
                        ((sbctemp & 0x100) != 0 ? Tables.FLAG_C : (byte)0)
                        | Tables.FLAG_N
                        | Tables.halfcarry_sub_table[lookup & 0x07]
                        | Tables.overflow_sub_table[lookup >> 4]
                        | Tables.sz53_table[cpu.af.a]
                     );
        }

        void SBC16(ushort hl, ushort value) {
            int sub16temp = cpu.hl.w - value - (cpu.af.f & Tables.FLAG_C);
            byte lookup = (byte)(
                            ((cpu.hl.w & 0x8800) >> 11)
                            | ((value & 0x8800) >> 10)
                            | ((sub16temp & 0x8800) >> 9)
                        );
            cpu.memptr.w = (ushort)(hl + 1);
            cpu.hl.w = (ushort)sub16temp;
            cpu.af.f = (byte)(
                        ((sub16temp & 0x10000) != 0 ? Tables.FLAG_C : (byte)0)
                        | Tables.FLAG_N
                        | Tables.overflow_sub_table[lookup >> 4]
                        | (cpu.hl.h & (Tables.FLAG_3 | Tables.FLAG_5 | Tables.FLAG_S))
                        | Tables.halfcarry_sub_table[lookup & 0x07]
                        | (cpu.hl.w != 0 ? (byte)0 : Tables.FLAG_Z)
                    );
        }

        void SLA(ref byte value) {
            cpu.af.f = (byte)(value >> 7);
            value <<= 1;
            cpu.af.f |= Tables.sz53p_table[value];
        }

        void SLL(ref byte value) {
            cpu.af.f = (byte)(value >> 7);
            value = (byte)((value << 1) | 0x01);
            cpu.af.f |= Tables.sz53p_table[(value)];
        }

        void SRA(ref byte value) {
            cpu.af.f = (byte)(value & Tables.FLAG_C);
            value = (byte)((value & 0x80) | (value >> 1));
            cpu.af.f |= Tables.sz53p_table[value];
        }

        void SRL(ref byte value) {
            cpu.af.f = (byte)(value & Tables.FLAG_C);
            value >>= 1;
            cpu.af.f |= Tables.sz53p_table[value];
        }

        void SUB(byte value) {
            int subtemp = cpu.af.a - value;
            byte lookup = (byte)(
                ((cpu.af.a & 0x88) >> 3)
                | ((value & 0x88) >> 2)
                | ((subtemp & 0x88) >> 1)
                );
            cpu.af.a = (byte)subtemp;
            cpu.af.f = (byte)(
                ((subtemp & 0x100) != 0 ? Tables.FLAG_C : (byte)0)
                | Tables.FLAG_N
                | Tables.halfcarry_sub_table[lookup & 0x07]
                | Tables.overflow_sub_table[lookup >> 4]
                | Tables.sz53_table[cpu.af.a]
                );
        }

        void XOR(byte value) {
            cpu.af.a ^= value;
            cpu.af.f = Tables.sz53p_table[cpu.af.a];
        }

        void RRD(int rd, int wr) {
            byte bytetemp = READ_MEM(cpu.hl.w, rd);
            WRITE_MEM(cpu.hl.w, (byte)((cpu.af.a << 4) | (bytetemp >> 4)), wr);
            cpu.af.a = (byte)((cpu.af.a & 0xf0) | (bytetemp & 0x0f));
            cpu.af.f = (byte)((cpu.af.f & Tables.FLAG_C) | Tables.sz53p_table[cpu.af.a]);
            cpu.memptr.w = (ushort)(cpu.hl.w + 1);
        }

        void RLD(int rd, int wr) {
            byte bytetemp = READ_MEM(cpu.hl.w, rd);
            WRITE_MEM(cpu.hl.w, (byte)((bytetemp << 4) | (cpu.af.a & 0x0f)), wr);
            cpu.af.a = (byte)((cpu.af.a & 0xf0) | (bytetemp >> 4));
            cpu.af.f = (byte)((cpu.af.f & Tables.FLAG_C) | Tables.sz53p_table[cpu.af.a]);
            cpu.memptr.w = (ushort)(cpu.hl.w + 1);
        }

        // ReSharper disable once UnusedMember.Local
        void IM(byte mode) {
            cpu.im = (IM_MODE)mode;
        }

        void IM(IM_MODE mode) {
            cpu.im = mode;
        }

        void LD_A_R() {
            cpu.af.a = (byte)((cpu.r & 0x7f) | (cpu.r7 & 0x80));
            cpu.af.f = (byte)(
                (cpu.af.f & Tables.FLAG_C)
                | Tables.sz53_table[cpu.af.a]
                | (cpu.iff2 != 0 ? Tables.FLAG_V : (byte)0)
                );
            cpu.reset_PV_on_int = true;
        }

        void LD_R_A() {
            cpu.r = cpu.r7 = cpu.af.a;
        }

        void LD_A_I() {
            cpu.af.a = cpu.i;
            cpu.af.f = (byte)(
                (cpu.af.f & Tables.FLAG_C)
                | Tables.sz53_table[cpu.af.a]
                | (cpu.iff2 != 0 ? Tables.FLAG_V : (byte)0)
                );
            cpu.reset_PV_on_int = true;
        }

        void NEG() {
            byte bytetemp = cpu.af.a;
            cpu.af.a = 0;
            SUB(bytetemp);
        }

        void RETI(int rd1, int rd2) {
            RetiEvent handler = Reti;

            cpu.iff1 = cpu.iff2;
            RET(rd1, rd2);
            if(handler != null) handler(this, null);
        }

        /*same as RETI, only opcode is different*/
        void RETN(int rd1, int rd2) {
            cpu.iff1 = cpu.iff2;
            RET(rd1, rd2);
        }

        void LDI(int rd, int wr) {
            byte bytetemp = READ_MEM(cpu.hl.w, rd);
            cpu.bc.w--;
            WRITE_MEM(cpu.de.w, bytetemp, wr);
            cpu.de.w++; cpu.hl.w++;
            bytetemp += cpu.af.a;
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_C | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.bc.w != 0 ? Tables.FLAG_V : (byte)0)
                | (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );
        }

        void CPI(int rd) {
            byte value = READ_MEM(cpu.hl.w, rd);
            byte bytetemp = (byte)(cpu.af.a - value);
            byte lookup = (byte)(
                ((cpu.af.a & 0x08) >> 3)
                | ((value & 0x08) >> 2)
                | ((bytetemp & 0x08) >> 1)
                );
            cpu.hl.w++; cpu.bc.w--;
            cpu.af.f = (byte)(
                (cpu.af.f & Tables.FLAG_C)
                | (cpu.bc.w != 0 ? (byte)(Tables.FLAG_V | Tables.FLAG_N) : Tables.FLAG_N)
                | Tables.halfcarry_sub_table[lookup]
                | (bytetemp != 0 ? (byte)0 : Tables.FLAG_Z)
                | (bytetemp & Tables.FLAG_S)
                );
            if((cpu.af.f & Tables.FLAG_H) != 0) bytetemp--;
            cpu.af.f |= (byte)(
                (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );
            cpu.memptr.w = (ushort)(cpu.memptr.w + 1);
        }

        /*undocumented flag effects for block output operations*/
        void OUT_BL(byte pbyte) {
            byte kval = (byte)(pbyte + cpu.hl.l);
            if((pbyte + cpu.hl.l) > 255) cpu.af.f |= (Tables.FLAG_C | Tables.FLAG_H);
            cpu.af.f |= Tables.parity_table[((kval & 7) ^ cpu.bc.b)];
        }

        /*undocumented flag effects for block input operations*/
        void IN_BL(byte pbyte, sbyte c_add) {
            byte kval = (byte)(pbyte + ((cpu.bc.c + c_add) & 0xff));
            if((pbyte + ((cpu.bc.c + c_add) & 0xff)) > 255) cpu.af.f |= (Tables.FLAG_C | Tables.FLAG_H);
            cpu.af.f |= Tables.parity_table[((kval & 7) ^ cpu.bc.b)];
        }

        void INI(int rd, int wr) {
            cpu.memptr.w = (ushort)(cpu.bc.w + 1);
            byte initemp = READ_PORT(cpu.bc.w, rd);
            WRITE_MEM(cpu.hl.w, initemp, wr);
            cpu.bc.b--; cpu.hl.w++;
            cpu.af.f = (byte)(
                ((initemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0)
                | Tables.sz53_table[cpu.bc.b]
                );
            IN_BL(initemp, 1);
        }

        void OUTI(int rd, int wr) {
            byte outitemp = READ_MEM(cpu.hl.w, rd);
            cpu.bc.b--;
            cpu.memptr.w = (ushort)(cpu.bc.w + 1);
            WRITE_PORT(cpu.bc.w, outitemp, wr);
            cpu.hl.w++;
            cpu.af.f = (byte)(((outitemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0) | Tables.sz53_table[cpu.bc.b]);
            OUT_BL(outitemp);
        }

        void LDD(int rd, int wr) {
            byte bytetemp = READ_MEM(cpu.hl.w, rd);
            cpu.bc.w--;
            WRITE_MEM(cpu.de.w, bytetemp, wr);
            cpu.de.w--; cpu.hl.w--;
            bytetemp += cpu.af.a;
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_C | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.bc.w != 0 ? Tables.FLAG_V : (byte)0)
                | (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );
        }

        void CPD(int rd) {
            byte value = READ_MEM(cpu.hl.w, rd);
            byte bytetemp = (byte)(cpu.af.a - value);
            byte lookup = (byte)(
                ((cpu.af.a & 0x08) >> 3)
                | ((value & 0x08) >> 2)
                | ((bytetemp & 0x08) >> 1)
                );
            cpu.hl.w--; cpu.bc.w--;
            cpu.af.f = (byte)(
                (cpu.af.f & Tables.FLAG_C)
                | (cpu.bc.w != 0 ? (byte)(Tables.FLAG_V | Tables.FLAG_N) : Tables.FLAG_N)
                | Tables.halfcarry_sub_table[lookup]
                | (bytetemp != 0 ? (byte)0 : Tables.FLAG_Z)
                | (bytetemp & Tables.FLAG_S)
                );
            if((cpu.af.f & Tables.FLAG_H) != 0) bytetemp--;
            cpu.af.f |= (byte)(
                (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );
            cpu.memptr.w = (ushort)(cpu.memptr.w - 1);
        }

        void IND(int rd, int wr) {
            cpu.memptr.w = (ushort)(cpu.bc.w - 1);
            byte initemp = READ_PORT(cpu.bc.w, rd);
            WRITE_MEM(cpu.hl.w, initemp, wr);
            cpu.bc.b--; cpu.hl.w--;
            cpu.af.f = (byte)(
                ((initemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0)
                | Tables.sz53_table[cpu.bc.b]
                );
            IN_BL(initemp, -1);
        }

        void OUTD(int rd, int wr) {
            byte outitemp = READ_MEM(cpu.hl.w, rd);
            cpu.bc.b--;
            cpu.memptr.w = (ushort)(cpu.bc.w - 1);
            WRITE_PORT(cpu.bc.w, outitemp, wr);
            cpu.hl.w--;
            cpu.af.f = (byte)(((outitemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0) | Tables.sz53_table[cpu.bc.b]);
            OUT_BL(outitemp);
        }

        void LDIR(int t1, int t2, int rd, int wr) {
            byte bytetemp = READ_MEM(cpu.hl.w, rd);
            WRITE_MEM(cpu.de.w, bytetemp, wr);
            cpu.hl.w++; cpu.de.w++; cpu.bc.w--;
            bytetemp += cpu.af.a;
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_C | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.bc.w != 0 ? Tables.FLAG_V : (byte)0)
                | (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );
            if(cpu.bc.w != 0) {
                cpu.pc.w -= 2;
                T_WAIT_UNTIL(t2);
                cpu.memptr.w = (ushort)(cpu.pc.w + 1);
            } else {
                T_WAIT_UNTIL(t1);
            }
        }

        void CPIR(int t1, int t2, int rd) {
            byte value = READ_MEM(cpu.hl.w, rd);
            byte bytetemp = (byte)(cpu.af.a - value);
            byte lookup = (byte)(
                ((cpu.af.a & 0x08) >> 3)
                | ((value & 0x08) >> 2)
                | ((bytetemp & 0x08) >> 1)
                );
            cpu.hl.w++; cpu.bc.w--;
            cpu.af.f = (byte)(
                (cpu.af.f & Tables.FLAG_C)
                | (cpu.bc.w != 0 ? (byte)(Tables.FLAG_V | Tables.FLAG_N) : Tables.FLAG_N)
                | Tables.halfcarry_sub_table[lookup]
                | (bytetemp != 0 ? (byte)0 : Tables.FLAG_Z)
                | (bytetemp & Tables.FLAG_S)
                );

            if((cpu.af.f & Tables.FLAG_H) != 0) bytetemp--;
            cpu.af.f |= (byte)(
                (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );

            if((cpu.af.f & (Tables.FLAG_V | Tables.FLAG_Z)) == Tables.FLAG_V) {
                cpu.pc.w -= 2;
                cpu.memptr.w = (ushort)(cpu.pc.w + 1);
                T_WAIT_UNTIL(t2);
            } else {
                cpu.memptr.w = (ushort)(cpu.memptr.w + 1);
                T_WAIT_UNTIL(t1);
            }
        }

        void INIR(int t1, int t2, int rd, int wr) {
            byte initemp = READ_PORT(cpu.bc.w, rd);
            WRITE_MEM(cpu.hl.w, initemp, wr);
            cpu.memptr.w = (ushort)(cpu.bc.w + 1);
            cpu.bc.b--; cpu.hl.w++;
            cpu.af.f = (byte)(
                ((initemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0)
                | Tables.sz53_table[cpu.bc.b]
                );
            if(cpu.bc.b != 0) {
                cpu.pc.w -= 2;
                T_WAIT_UNTIL(t2);
            } else {
                T_WAIT_UNTIL(t1);
            }
            IN_BL(initemp, 1);
        }

        void OTIR(int t1, int t2, int rd, int wr) {
            byte outitemp = READ_MEM(cpu.hl.w, rd);
            cpu.bc.b--;
            cpu.memptr.w = (ushort)(cpu.bc.w + 1);
            WRITE_PORT(cpu.bc.w, outitemp, wr);
            cpu.hl.w++;
            cpu.af.f = (byte)(
                ((outitemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0)
                | Tables.sz53_table[cpu.bc.b]
                );
            if(cpu.bc.b != 0) {
                cpu.pc.w -= 2;
                T_WAIT_UNTIL(t2);
            } else {
                T_WAIT_UNTIL(t1);
            }
            OUT_BL(outitemp);
        }

        void LDDR(int t1, int t2, int rd, int wr) {
            byte bytetemp = READ_MEM(cpu.hl.w, rd);
            WRITE_MEM(cpu.de.w, bytetemp, wr);
            cpu.hl.w--; cpu.de.w--; cpu.bc.w--;
            bytetemp += cpu.af.a;
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_C | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.bc.w != 0 ? Tables.FLAG_V : (byte)0)
                | (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );

            if(cpu.bc.w != 0) {
                cpu.pc.w -= 2;
                T_WAIT_UNTIL(t2);
                cpu.memptr.w = (ushort)(cpu.pc.w + 1);
            } else {
                T_WAIT_UNTIL(t1);
            }
        }

        void CPDR(int t1, int t2, int rd) {
            byte value = READ_MEM(cpu.hl.w, rd);
            byte bytetemp = (byte)(cpu.af.a - value);
            byte lookup = (byte)(
                ((cpu.af.a & 0x08) >> 3)
                | ((value & 0x08) >> 2)
                | ((bytetemp & 0x08) >> 1)
                );
            cpu.hl.w--; cpu.bc.w--;
            cpu.af.f = (byte)(
                (cpu.af.f & Tables.FLAG_C)
                | (cpu.bc.w != 0 ? (byte)(Tables.FLAG_V | Tables.FLAG_N) : Tables.FLAG_N)
                | Tables.halfcarry_sub_table[lookup]
                | (bytetemp != 0 ? (byte)0 : Tables.FLAG_Z)
                | (bytetemp & Tables.FLAG_S)
                );

            if((cpu.af.f & Tables.FLAG_H) != 0) bytetemp--;
            cpu.af.f |= (byte)(
                (bytetemp & Tables.FLAG_3)
                | ((bytetemp & 0x02) != 0 ? Tables.FLAG_5 : (byte)0)
                );
            if((cpu.af.f & (Tables.FLAG_V | Tables.FLAG_Z)) == Tables.FLAG_V) {
                cpu.pc.w -= 2;
                cpu.memptr.w = (ushort)(cpu.pc.w + 1);
                T_WAIT_UNTIL(t2);
            } else {
                cpu.memptr.w = (ushort)(cpu.memptr.w - 1);
                T_WAIT_UNTIL(t1);
            }
        }

        void INDR(int t1, int t2, int rd, int wr) {
            byte initemp = READ_PORT(cpu.bc.w, rd);
            WRITE_MEM(cpu.hl.w, initemp, wr);
            cpu.memptr.w = (ushort)(cpu.bc.w - 1);
            cpu.bc.b--; cpu.hl.w--;
            cpu.af.f = (byte)(
                ((initemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0)
                | Tables.sz53_table[cpu.bc.b]
                );
            if(cpu.bc.b != 0) {
                cpu.pc.w -= 2;
                T_WAIT_UNTIL(t2);
            } else {
                T_WAIT_UNTIL(t1);
            }
            IN_BL(initemp, -1);
        }

        void OTDR(int t1, int t2, int rd, int wr) {
            byte outitemp = READ_MEM(cpu.hl.w, rd);
            cpu.bc.b--;
            cpu.memptr.w = (ushort)(cpu.bc.w - 1);
            WRITE_PORT(cpu.bc.w, outitemp, wr);
            cpu.hl.w--;
            cpu.af.f = (byte)(
                ((outitemp & 0x80) != 0 ? Tables.FLAG_N : (byte)0)
                | Tables.sz53_table[cpu.bc.b]
                );
            if(cpu.bc.b != 0) {
                cpu.pc.w -= 2;
                T_WAIT_UNTIL(t2);
            } else {
                T_WAIT_UNTIL(t1);
            }
            OUT_BL(outitemp);
        }

        void RLCA() {
            cpu.af.a = (byte)((cpu.af.a << 1) | (cpu.af.a >> 7));
            cpu.af.f = (byte)(
                    (cpu.af.f & (Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                    | (cpu.af.a & (Tables.FLAG_C | Tables.FLAG_3 | Tables.FLAG_5))
                );
        }

        void RRCA() {
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.af.a & Tables.FLAG_C)
                );
            cpu.af.a = (byte)((cpu.af.a >> 1) | (cpu.af.a << 7));
            cpu.af.f |= (byte)((cpu.af.a & (Tables.FLAG_3 | Tables.FLAG_5)));
        }

        void DJNZ(int offset, int t1, int t2) {
            cpu.bc.b--;
            if(cpu.bc.b != 0) {
                cpu.pc.w = (ushort)(cpu.pc.w + offset);
                cpu.memptr.w = cpu.pc.w;
                T_WAIT_UNTIL(t2);
            } else {
                T_WAIT_UNTIL(t1);
            }
        }

        void RLA() {
            byte bytetemp = cpu.af.a;
            cpu.af.a = (byte)((cpu.af.a << 1) | (cpu.af.f & Tables.FLAG_C));
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.af.a & (Tables.FLAG_3 | Tables.FLAG_5)) | (bytetemp >> 7)
                );
        }

        void RRA() {
            byte bytetemp = cpu.af.a;
            cpu.af.a = (byte)((cpu.af.a >> 1) | (cpu.af.f << 7));
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.af.a & (Tables.FLAG_3 | Tables.FLAG_5)) | (bytetemp & Tables.FLAG_C)
              );
        }

        void DAA() {
            int index = (cpu.af.a + 0x100 * ((cpu.af.f & 3) + ((cpu.af.f >> 2) & 4))) * 2;
            cpu.af.f = Tables.daatab[index];
            cpu.af.a = Tables.daatab[index + 1];
        }

        void EX(ref RegPair rp1, ref RegPair rp2) {
            ushort tmp = rp1.w;
            rp1.w = rp2.w;
            rp2.w = tmp;
        }

        void EX_MPTR(ref RegPair rp1, ref RegPair rp2) {
            ushort wordtemp = rp1.w; rp1.w = rp2.w; rp2.w = wordtemp;
            cpu.memptr.w = wordtemp;
        }

        void CPL() {
            cpu.af.a ^= 0xff;
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_C | Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.af.a & (Tables.FLAG_3 | Tables.FLAG_5)) | (Tables.FLAG_N | Tables.FLAG_H)
                );
        }

        void SCF() {
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                | (cpu.af.a & (Tables.FLAG_3 | Tables.FLAG_5))
                | Tables.FLAG_C
                );
        }

        void CCF() {
            cpu.af.f = (byte)(
                (cpu.af.f & (Tables.FLAG_P | Tables.FLAG_Z | Tables.FLAG_S))
                | ((cpu.af.f & Tables.FLAG_C) != 0 ? Tables.FLAG_H : Tables.FLAG_C)
                | (cpu.af.a & (Tables.FLAG_3 | Tables.FLAG_5))
                );
        }

        void HALT() {
            cpu.halted = true;
            cpu.pc.w--;
        }

        void EXX() {
            ushort wordtemp = cpu.bc.w; cpu.bc.w = cpu.bc_.w; cpu.bc_.w = wordtemp;
            wordtemp = cpu.de.w; cpu.de.w = cpu.de_.w; cpu.de_.w = wordtemp;
            wordtemp = cpu.hl.w; cpu.hl.w = cpu.hl_.w; cpu.hl_.w = wordtemp;
        }

        void DI() {
            cpu.iff1 = cpu.iff2 = 0;
        }

        void EI() {
            cpu.iff1 = cpu.iff2 = 1;
            cpu.noint_once = true;
        }

        void SET(byte bit, ref byte val) {
            val |= (byte)(1 << bit);
        }

        void RES(byte bit, ref byte val) {
            val &= (byte)~(1 << bit);
        }

    } // class
} // namespace
