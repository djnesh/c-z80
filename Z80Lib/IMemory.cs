﻿namespace Z80Lib {
    public interface IMemory {
        byte Read(Z80 cpu, ushort addr, bool M1State = false);
        void Write(Z80 cpu, ushort addr, byte value);
    }
}
