﻿using System;

namespace Z80Lib {
    /**
     * to be called on each T-State [optional]
     */
    public delegate void TStateEvent(object sender, EventArgs e);
    /**
     * called when the RETI instruction is executed (useful for emulating Z80 PIO/CTC and such)
     */
    public delegate void RetiEvent(object sender, EventArgs e);

    public enum IM_MODE { IM0 = 0, IM1 = 1, IM2 = 2};

    // What's stored in the main processor
    public struct Context {
        public RegPair af, bc, de, hl;
        public RegPair af_, bc_, de_, hl_;
        public RegPair ix, iy;
        public byte i;
        public ushort r; // The low seven bits of the R register. 16 bits long so it can also act as an RZX instruction counter
        public byte r7; // The high bit of the R register
        public RegPair sp, pc;
        public byte iff1, iff2;
        public RegPair memptr; // undocumented internal register
        public IM_MODE im;
        public bool halted;

        public ulong tstate; // t-state clock of current/last step
        public byte op_tstate; // clean (without WAITs and such) t-state of currently executing instruction

        public bool noint_once; // disable interrupts before next opcode?
        public bool reset_PV_on_int; // reset P/V flag on interrupt? (for LD A,R / LD A,I)
        public bool doing_opcode; // is there an opcode currently executing?
        public byte int_vector_req; // opcode must be fetched from IO device? (int vector read)
        public byte prefix;

        public RegPair tmpword, tmpaddr;
        public byte tmpbyte;
        public sbyte tmpbyte_s;

        public IMemory mem;
        public IInputOutputPort io;
        public IIRQRead intread;

        // real R value r/o
        public byte R {
            get {
                return (byte)((r7 & 0x80) | (r & 0x7F));
            }
        }

        public void Reset() {
            pc.w = 0x0000;

            iff1 = iff2 = 0;
            im = IM_MODE.IM0;

            af.w = af_.w = 0xFFFF;
            bc.w = bc_.w = 0xFFFF;
            de.w = de_.w = 0xFFFF;
            hl.w = hl_.w = 0xFFFF;
            ix.w = iy.w = 0xFFFF;

            sp.w = 0xFFFF;

            r = 0;
            i = r7 = 0;

            noint_once = reset_PV_on_int = halted = false;
            int_vector_req = 0;
            doing_opcode = false;
            tstate = op_tstate = 0;
            prefix = 0;
        }

        public string ToExpectedString() {
            return "";
        }

        public override string ToString() {
            string ret = string.Format("PC {0} SP {1}\n", pc, sp);
            ret += string.Format("AF {0} BC {1} DE {2} HL {3}\n", af, bc, de, hl);
            ret += string.Format("AF' {0} BC' {1} DE' {2} HL' {3}\n", af_, bc_, de_, hl_);
            ret += string.Format("IX {0} IY {1}\n", ix, iy);
            ret += string.Format("R {0:X2} r {1:X2} r7 {2:X2}\n", R, r, r7);
            ret += string.Format("I {0} IFF1 {1} IFF2 {2} {3} H {4}", i, iff1, iff2, im, halted);
            return ret;
        }
    }
}