﻿using System;
using System.Collections.Generic;
using System.IO;
using Z80Lib;
using Z80LibTest.Properties;

namespace Z80LibTest {
    internal class ZXTestData {
        internal Context ctx;
        internal ulong endstates;
        internal readonly Dictionary<ushort, byte> mem;

        public ZXTestData() {
            ctx = new Context();
            ctx.Reset();
            mem = new Dictionary<ushort, byte>();
        }

        public override string ToString() {
            string ret = ctx.ToString();
            foreach(KeyValuePair<ushort, byte> item in mem) {
                ret += string.Format("\n{0:X4}: {1:X2}", item.Key, item.Value);
            }
            return ret;
        }
    }

    class Tester {
        private readonly Dictionary<string, ZXTestData> testsIn;
        private readonly Dictionary<string, ZXTestData> testsExpected;

        public Tester() {
            testsIn = new Dictionary<string, ZXTestData>();
            testsExpected = new Dictionary<string, ZXTestData>();

            Console.WriteLine("Loading tests.in");
            StringReader fh = new StringReader(Resources.tests_in);
            while(fh.Peek() != -1) {
                loadTest(fh, testsIn);
            }

            Console.WriteLine("Loading tests.expected");
            fh = new StringReader(Resources.tests_expected);
            while(fh.Peek() != -1) {
                loadTest(fh, testsExpected);
            }
        }

        public void Run() {
            TestInputOutputPort port = new TestInputOutputPort();
            int good = 0, bad = 0, total = 0;

            Console.WriteLine("Running Z80 Tests");
            foreach(KeyValuePair<string, ZXTestData> item in testsIn) {
                Console.WriteLine("--- START '{0}' ---", item.Key);
                total++;

                ZXTestData data = item.Value;
                ZXTestData expected = testsExpected[item.Key];

                data.ctx.mem = new TestMemory(data);
                data.ctx.io = port;
                data.ctx.intread = port;
                Z80 z80 = new Z80(data.ctx);

                //Console.WriteLine(data);
                //Console.WriteLine("Running until T: {0}", data.endstates);

                ulong t = 0;
                while((t <= data.endstates - 1) || !z80.InstructionDone()) {
                    t += z80.Step();
                }

                // check registers
                bool passed = true;

                passed &= compareValue("T", t, expected.endstates);
                passed &= compareRegisters(z80.getContext(), expected.ctx);
                TestMemory m = (TestMemory)z80.getContext().mem;
                passed &= compareMemory(m.GetDiff(), expected.mem);

                if(!passed) {
                    Console.WriteLine("--- FAILED'{0}' ---\n", item.Key);
                    bad++;
                    Console.WriteLine("--- CURRENT ---");
                    Console.WriteLine(z80.getContext());

                    Console.WriteLine("--- EXPECTED ---");
                    Console.WriteLine(expected.ctx);
                    break;
                } else {
                    good++;
                }

                Console.WriteLine("Runtime {0}T", t);
                Console.WriteLine("--- END   '{0}' ---\n", item.Key);
            }
            Console.WriteLine("\nRan {0} tests, passed {1}, failed {2}.", total, good, bad);
        }

        #region Registers
        static bool compareRegisters(Context ctx, Context expected) {
            bool ret = true;

            ret &= compareRegisterPair("PC", ctx.pc, expected.pc);
            ret &= compareRegisterPair("SP", ctx.sp, expected.sp);

            ret &= compareRegisterPair("AF", ctx.af, expected.af);
            ret &= compareRegisterPair("BC", ctx.bc, expected.bc);
            ret &= compareRegisterPair("DE", ctx.de, expected.de);
            ret &= compareRegisterPair("HL", ctx.hl, expected.hl);

            ret &= compareRegisterPair("AF'", ctx.af_, expected.af_);
            ret &= compareRegisterPair("BC'", ctx.bc_, expected.bc_);
            ret &= compareRegisterPair("DE'", ctx.de_, expected.de_);
            ret &= compareRegisterPair("HL'", ctx.hl_, expected.hl_);

            ret &= compareRegisterPair("IX", ctx.ix, expected.ix);
            ret &= compareRegisterPair("IY", ctx.iy, expected.iy);

            ret &= compareValue("I", ctx.i, expected.i);
            ret &= compareValue("R", ctx.R, expected.r);

            ret &= compareValue("IFF1", ctx.iff1, expected.iff1);
            ret &= compareValue("IFF2", ctx.iff2, expected.iff2);
            ret &= compareValue("IM", ctx.im, expected.im);
            ret &= compareValue("HALTED", ctx.halted, expected.halted);

            return ret;
        }

        static bool compareRegisterPair(string name, RegPair got, RegPair expected) {
            if(got == expected) return true;
            Console.WriteLine("{0}: Got {1} expected {2}", name, got, expected);
            return false;
        }

        static bool compareValue(string name, IM_MODE got, IM_MODE expected) {
            if(got == expected) return true;
            Console.WriteLine("{0}: Got {1} expected {2}", name, got, expected);
            return false;
        }

        static bool compareValue(string name, bool got, bool expected) {
            if(got == expected) return true;
            Console.WriteLine("{0}: Got {1} expected {2}", name, got, expected);
            return false;
        }

        static bool compareValue(string name, ulong got, ulong expected) {
            if(got == expected) return true;
            Console.WriteLine("{0}: Got {1} expected {2}", name, got, expected);
            return false;
        }
        #endregion

        static bool compareMemory(Dictionary<ushort, byte> inDiff, Dictionary<ushort, byte> expectedDiff) {
            bool ret = true;

            if(inDiff.Count != expectedDiff.Count) {
                Console.WriteLine("Memory: size mismatch");
                return false;
            }

            foreach(KeyValuePair<ushort, byte> item in inDiff) {
                if(item.Value == expectedDiff[item.Key]) continue;
                ret = false;
                Console.WriteLine("Memory: {0:X4} expected {1:X2} got {2:X2}", item.Key, expectedDiff[item.Key], item.Value);
            }

            return ret;
        }

        static void loadTest(TextReader fh, Dictionary<string, ZXTestData> t) {
            if(fh.Peek() < 0) {
                return;
            }

            ZXTestData data = new ZXTestData();

            string test_name = fh.ReadLine();
            while(fh.Peek() == ' ') fh.ReadLine(); // skip M? lines

            // registers
            string line = fh.ReadLine();
            string[] fields = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            //Console.WriteLine(line);
            //Console.WriteLine(string.Join(",", fields));
            int f = 0;
            data.ctx.af.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.bc.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.de.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.hl.w = Convert.ToUInt16(fields[f++], 16);

            data.ctx.af_.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.bc_.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.de_.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.hl_.w = Convert.ToUInt16(fields[f++], 16);

            data.ctx.ix.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.iy.w = Convert.ToUInt16(fields[f++], 16);

            data.ctx.sp.w = Convert.ToUInt16(fields[f++], 16);
            data.ctx.pc.w = Convert.ToUInt16(fields[f++], 16);

            // 2nd line
            line = fh.ReadLine();
            fields = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            //Console.WriteLine(line);
            //Console.WriteLine(string.Join(",", fields));
            f = 0;
            data.ctx.i = (byte)Convert.ToUInt16(fields[f++], 16);
            data.ctx.r = Convert.ToUInt16(fields[f++], 16);
            data.ctx.r7 = (byte)data.ctx.r;
            data.ctx.iff1 = (byte)Convert.ToUInt16(fields[f++]);
            data.ctx.iff2 = (byte)Convert.ToUInt16(fields[f++]);
            data.ctx.im = (IM_MODE)Convert.ToUInt16(fields[f++]);
            data.ctx.halted = Convert.ToUInt16(fields[f++]) != 0;
            data.endstates = Convert.ToUInt16(fields[f++]);

            // memory
            while(fh.Peek() != -1) {
                line = fh.ReadLine();
                if(line == "-1") {
                    fh.ReadLine();
                    break;
                }
                if(line == "") break;
                fields = line.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                //Console.WriteLine(line);
                //Console.WriteLine(string.Join(",", fields));
                ushort start = Convert.ToUInt16(fields[0], 16);
                for(int i = 1; i < fields.Length; i++) {
                    if(fields[i] == "-1") break;
                    data.mem[(ushort)(start + i - 1)] = (byte)Convert.ToUInt16(fields[i], 16);
                }
            }

            t[test_name] = data;
        }
    }

    // ReSharper disable once ClassNeverInstantiated.Global
    class Z80LibTest {

        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args) {
            Tester test = new Tester();
            test.Run();
        }
    }
}
