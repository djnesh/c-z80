﻿using System;
using Z80Lib;

namespace Z80LibTest {
    /**
     * Simple IO class for testing
     */
    class TestInputOutputPort : IInputOutputPort, IIRQRead {
        public byte Read(Z80 cpu, ushort port) {
            byte ret = (byte)(port >> 8);
            //Console.WriteLine("{0,5:D} PR {1:X4} {2:X2}", cpu.getContext().tstate, port, ret));
            return ret; // fuse uses this value for testing
        }

        public void Write(Z80 cpu, ushort port, byte val) {
            //Console.WriteLine("{0,5:D} PR {1:X4} {2:X2}", cpu.getContext().tstate, port, val);
        }

        // IRQ read
        public byte Read(Z80 cpu) {
            Console.WriteLine("{0,5:D} IR {1:X4} {2:X2}", cpu.getContext().tstate, 0, 0xFF);
            return 0xFF;
        }
    }
}
