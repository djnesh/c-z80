﻿using System;
using System.Collections.Generic;
using Z80Lib;

namespace Z80LibTest {
    /**
     * Simple memory for testing
     */
    internal class TestMemory : IMemory {
        const int MemSize = 0x10000;
        readonly byte[] mem = new byte[MemSize];
        readonly byte[] initialState = new byte[MemSize];

        public TestMemory(ZXTestData src) {
            // init
            for(int i = 0; i < MemSize; i += 4) {
                mem[i] = 0xde; mem[i + 1] = 0xad;
                mem[i + 2] = 0xbe; mem[i + 3] = 0xef;
            }

            // load data
            foreach(KeyValuePair<ushort, byte> item in src.mem) {
                mem[item.Key] = item.Value;
            }

            Array.Copy(mem, initialState, MemSize);
        }

        internal Dictionary<ushort, byte> GetDiff() {
            Dictionary<ushort, byte> ret = new Dictionary<ushort, byte>();
            for(int i = 0; i < MemSize; i++) {
                if(mem[i] == initialState[i]) continue;
                ret[(ushort)i] = mem[i];
            }
            return ret;
        }

        public byte Read(Z80 cpu, ushort addr, bool m1) {
            string m = m1 ? "M1" : "MR";
            Console.WriteLine(
                "{0,5:D} {3} {1:X4} {2:X2}",
                cpu.getContext().tstate,
                addr,
                mem[addr],
                m
                );
            return mem[addr];
        }

        public void Write(Z80 cpu, ushort addr, byte val) {
            Console.WriteLine("{0,5:D} MW {1:X4} {2:X2}", cpu.getContext().tstate, addr, val);
            mem[addr] = val;
        }
    }
}
